function createCommonjsModule(e, t) {
  return e((t = { exports: {} }), t.exports), t.exports;
}
function getCjsExportFromNamespace(e) {
  return (e && e.default) || e;
}
function _typeof(e) {
  return (_typeof =
    "function" == typeof Symbol && "symbol" == typeof Symbol.iterator
      ? function (e) {
          return typeof e;
        }
      : function (e) {
          return e &&
            "function" == typeof Symbol &&
            e.constructor === Symbol &&
            e !== Symbol.prototype
            ? "symbol"
            : typeof e;
        })(e);
}
require("setimmediate"),
  (function () {
    var e = require("util"),
      t = require("events");
    require("stream").Transform || require("readable-stream/transform");
    var a = require("archiver"),
      i = require("fs"),
      r =
        require("stream").PassThrough || require("readable-stream/passthrough"),
      s = { settings: {}, types: {}, docPrototypes: {}, resParserTypes: {} },
      n = function e(n) {
        if (this instanceof e == !1) return new e(n);
        t.EventEmitter.call(this);
        var o = this,
          l = {};
        return (
          (l.features = {}),
          (l.features.type = {}),
          (l.features.outputType = "zip"),
          (l.pages = []),
          (l.resources = []),
          (l.type = {}),
          (l.plugs = {
            intAddAnyResourceToParse: function (e, t, a, i, r, n) {
              var o = {};
              (o.name = e),
                (o.type = t),
                (o.data = a),
                (o.callback = i),
                (o.is_perment = r),
                (o.removed_after_used = n || !1),
                s.settings.verbose &&
                  console.log("[officegen] Push new res : ", o),
                l.resources.push(o);
            },
            type: {},
          }),
          (this.generate = function (e, t) {
            s.settings.verbose &&
              console.log("[officegen] Start generate() : ", {
                outputType: l.features.outputType,
              }),
              "object" === _typeof(t) &&
                (t.finalize && o.on("finalize", t.finalize),
                t.error && o.on("error", t.error)),
              l.features.page_name &&
                0 === l.pages.length &&
                o.emit(
                  "error",
                  "ERROR: No " +
                    l.features.page_name +
                    " been found inside your document."
                ),
              o.emit("beforeGen", l);
            var n = a("zip" === l.features.outputType ? "zip" : "tar");
            if (
              (n.on("error", function (e) {
                o.emit("error", e);
              }),
              "gzip" === l.features.outputType)
            ) {
              var p = require("zlib").createGzip();
              n.pipe(p).pipe(e);
            } else n.pipe(e);
            !(function e(t) {
              var a;
              if (
                (s.settings.verbose &&
                  console.log(
                    "[officegen] generateNextResource(" + t + ") : ",
                    l.resources[t]
                  ),
                l.resources.length > t)
              )
                if (void 0 !== l.resources[t]) {
                  switch (l.resources[t].type) {
                    case "buffer":
                      a = l.resources[t].callback(l.resources[t].data);
                      break;
                    case "file":
                      a = i.createReadStream(
                        l.resources[t].data || l.resources[t].name
                      );
                      break;
                    case "stream":
                      a = l.resources[t].data;
                      break;
                    case "officegen":
                      (a = new r()), l.resources[t].data.generate(a);
                      break;
                    default:
                      for (var p in s.resParserTypes)
                        if (
                          p === l.resources[t].type &&
                          s.resParserTypes[p] &&
                          s.resParserTypes[p].parserFunc
                        ) {
                          a = s.resParserTypes[p].parserFunc(
                            o,
                            l.resources[t].name,
                            l.resources[t].callback,
                            l.resources[t].data,
                            s.resParserTypes[p].extra_data
                          );
                          break;
                        }
                  }
                  void 0 !== a
                    ? (s.settings.verbose &&
                        console.log(
                          '[officegen] Adding into archive : "' +
                            l.resources[t].name +
                            '" (' +
                            l.resources[t].type +
                            ")..."
                        ),
                      n.append(a, { name: l.resources[t].name }),
                      e(t + 1))
                    : (s.settings.verbose &&
                        console.log("[officegen] resStream is undefined"),
                      e(t + 1));
                } else e(t + 1);
              else
                l.resources.forEach(function (e) {
                  if (e.removed_after_used) {
                    var t = e.data || e.name;
                    s.settings.verbose &&
                      console.log("[officegen] Removing resource: ", t),
                      i.unlinkSync(t);
                  }
                }),
                  s.settings.verbose &&
                    console.log("[officegen] Finalizing archive ..."),
                  n.finalize(),
                  o.emit("afterGen", l, null, n.pointer()),
                  o.emit("finalize", n.pointer());
            })(0);
          }),
          (this.startNewDoc = function () {
            for (var e = [], t = 0; l.resources.length > t; t++)
              l.resources[t].is_perment || e.push(t);
            for (t = 0; e.length > t; t++) l.resources.splice(e[t] - t, 1);
            (l.pages.length = 0), o.emit("clearDoc", l);
          }),
          (this.addResourceToParse = function (e, t, a, i) {
            l.plugs.intAddAnyResourceToParse(e, t, a, i, !1);
          }),
          "string" == typeof n && (n = { type: n }),
          (o.options = (function e(t, a) {
            var i,
              r = {
                boolean: !1,
                function: !0,
                object: !0,
                number: !1,
                string: !1,
                undefined: !1,
              };
            function s(e) {
              return !(!e || !r[_typeof(e)]);
            }
            for (
              var n = (t = t || {}),
                o = n,
                l = arguments,
                p = 0,
                c = l.length,
                d = -1,
                m =
                  r[_typeof(n)] &&
                  (function (e) {
                    return s(e) ? Object.keys(e) : [];
                  })(n),
                f = m ? m.length : 0;
              ++p < c;

            )
              if ((n = l[p]) && r[_typeof(n)])
                for (; ++d < f; )
                  null == o[(i = m[d])]
                    ? (o[i] = n[i])
                    : s(o[i]) && s(n[i]) && (o[i] = e(o[i], n[i]));
            return o;
          })(n, { type: "unknown" })),
          o.options && o.options.onerr && o.on("error", o.options.onerr),
          o.options && o.options.onend && o.on("finalize", o.options.onend),
          o.options.type &&
            (function (e) {
              l.length = 0;
              var t = !1;
              if (e) {
                for (var a in s.types)
                  if (a === e && s.types[a] && s.types[a].createFunc) {
                    s.types[a].createFunc(o, e, o.options, l, s.types[a]),
                      (t = !0);
                    break;
                  }
                t || o.emit("error", "FATAL ERROR: Invalid file type.");
              }
            })(o.options.type),
          this
        );
      };
    e.inherits(n, t.EventEmitter),
      (module.exports = function (e) {
        return new n(e);
      }),
      (module.exports.setVerboseMode = function (e) {
        s.settings.verbose = e;
      }),
      (module.exports.plugins = {
        registerDocType: function (e, t, a, i, r) {
          (s.types[e] = {}),
            (s.types[e].createFunc = t),
            (s.types[e].schema_data = a),
            (s.types[e].type = i),
            (s.types[e].display = r);
        },
        getDocTypeByName: function (e) {
          return s.types[e];
        },
        registerPrototype: function (e, t, a) {
          (s.docPrototypes[e] = {}),
            (s.docPrototypes[e].baseObj = t),
            (s.docPrototypes[e].display = a);
        },
        getPrototypeByName: function (e) {
          return s.docPrototypes[e];
        },
        registerParserType: function (e, t, a, i) {
          (s.resParserTypes[e] = {}),
            (s.resParserTypes[e].parserFunc = t),
            (s.resParserTypes[e].extra_data = a),
            (s.resParserTypes[e].display = i);
        },
        getVerboseMode: function (e, t) {
          return e || t
            ? (s.settings.verbose &&
                "object" === _typeof(s.settings.verbose) &&
                (e &&
                  s.settings.verbose.docType &&
                  "object" === _typeof(s.settings.verbose.docType) &&
                  s.settings.verbose.docType.indexOf &&
                  (a = s.settings.verbose.docType.indexOf(e) >= 0),
                !1 !== a &&
                  t &&
                  s.settings.verbose.moduleName &&
                  "object" === _typeof(s.settings.verbose.moduleName) &&
                  s.settings.verbose.moduleName.indexOf &&
                  (a = s.settings.verbose.moduleName.indexOf(t) >= 0)),
              !!a)
            : !!s.settings.verbose;
          var a;
        },
      }),
      (module.exports.schema = s.types),
      (module.exports.docType = { TEXT: 1, SPREADSHEET: 2, PRESENTATION: 3 });
  })();
var baseobj = Object.freeze({}),
  OfficeChart = require("./officechart.js"),
  msdoc = require("./msofficegen.js"),
  pptxShapes = require("./pptxshapes.js"),
  pptxFields = require("./pptxfields.js"),
  officeTable = require("./genofficetable"),
  path = require("path"),
  fast_image_size = require("fast-image-size"),
  excelbuilder = require("./msexcel-builder.js"),
  xmlBuilder = require("xmlbuilder"),
  docplugman = require("./docplug"),
  plugWidescreen = require("./pptxplg-widescreen"),
  plugSpeakernotes = require("./pptxplg-speakernotes"),
  plugLayouts = require("./pptxplg-layouts");
function encodeHTML(e) {
  return e
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;");
}
var GLOBAL_CHART_COUNT = 0;
function makePptx(e, t, a, i, r) {
  function s(e) {
    if (!e) return pptxShapes.RECTANGLE;
    if ("object" === _typeof(e) && e.name && e.displayName && e.avLst) return e;
    if (pptxShapes[e]) return pptxShapes[e];
    for (var t in pptxShapes) {
      if (pptxShapes[t].name === e) return pptxShapes[t];
      if (pptxShapes[t].displayName === e) return pptxShapes[t];
    }
    return pptxShapes.RECTANGLE;
  }
  function n(e) {
    (e && "object" === _typeof(e)) || (e = {});
    var t =
      '<a:p><a:r><a:rPr lang="en-US" smtClean="0"/><a:t>Click to edit Master title style</a:t></a:r><a:endParaRPr lang="en-US"/></a:p>';
    e.ph1 && e.slide && e.ph1.length && (t = c("", e.ph1, {}, e.slide));
    var a =
      '<a:p><a:r><a:rPr lang="en-US" smtClean="0"/><a:t>Click to edit Master subtitle style</a:t></a:r><a:endParaRPr lang="en-US"/></a:p>';
    e.ph2 && e.slide && e.ph2.length && (a = c("", e.ph2, {}, e.slide));
    var r = l("DATE_TIME", 1, e.useDate),
      s = "",
      n = 4;
    return (
      (!e.isDate && e.isRealSlide) ||
        ((s +=
          '<p:sp><p:nvSpPr><p:cNvPr id="' +
          n +
          '" name="Date Placeholder 3"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="dt" sz="half" idx="10"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:fld id="{F8166F1F-CE9B-4651-A6AA-CD717754106B}" type="datetimeFigureOut"><a:rPr lang="en-US" smtClean="0"/><a:t>' +
          r +
          '</a:t></a:fld><a:endParaRPr lang="en-US"/></a:p></p:txBody></p:sp>'),
        n++),
      e.isFooter && e.ft && e.slide && e.ft.length
        ? ((s +=
            '<p:sp><p:nvSpPr><p:cNvPr id="' +
            n +
            '" name="Footer Placeholder 4"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="ftr" sz="quarter" idx="11"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/>' +
            c("", e.ft, {}, e.slide) +
            "</p:txBody></p:sp>"),
          n++)
        : e.isRealSlide ||
          ((s +=
            '<p:sp><p:nvSpPr><p:cNvPr id="' +
            n +
            '" name="Footer Placeholder 4"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="ftr" sz="quarter" idx="11"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:endParaRPr lang="en-US"/></a:p></p:txBody></p:sp>'),
          n++),
      (!e.isSlideNum && e.isRealSlide) ||
        ((s +=
          '<p:sp><p:nvSpPr><p:cNvPr id="' +
          n +
          '" name="Slide Number Placeholder 5"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="sldNum" sz="quarter" idx="12"/></p:nvPr></p:nvSpPr><p:spPr/><p:txBody><a:bodyPr/><a:lstStyle/><a:p><a:fld id="{B1393E5F-521B-4CAD-9D3A-AE923D912DCE}" type="slidenum"><a:rPr lang="en-US" smtClean="0"/><a:t>‹#›</a:t></a:fld><a:endParaRPr lang="en-US"/></a:p></p:txBody></p:sp>'),
        n++),
      i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
        "<p:sld" +
        (e.isRealSlide ? "" : "Layout") +
        ' xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"' +
        (e.isRealSlide ? "" : ' type="title" preserve="1"') +
        '><p:cSld name="Title Slide"><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="2" name="Title 1"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="ctrTitle"/></p:nvPr></p:nvSpPr><p:spPr><a:xfrm><a:off x="685800" y="2130425"/><a:ext cx="7772400" cy="1470025"/></a:xfrm></p:spPr><p:txBody><a:bodyPr/><a:lstStyle/>' +
        t +
        '</p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="3" name="Subtitle 2"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="subTitle" idx="1"/></p:nvPr></p:nvSpPr><p:spPr><a:xfrm><a:off x="1371600" y="3886200"/><a:ext cx="6400800" cy="1752600"/></a:xfrm></p:spPr><p:txBody><a:bodyPr/><a:lstStyle><a:lvl1pPr marL="0" indent="0" algn="ctr"><a:buNone/><a:defRPr><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl1pPr><a:lvl2pPr marL="457200" indent="0" algn="ctr"><a:buNone/><a:defRPr><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl2pPr><a:lvl3pPr marL="914400" indent="0" algn="ctr"><a:buNone/><a:defRPr><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl3pPr><a:lvl4pPr marL="1371600" indent="0" algn="ctr"><a:buNone/><a:defRPr><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl4pPr><a:lvl5pPr marL="1828800" indent="0" algn="ctr"><a:buNone/><a:defRPr><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl5pPr><a:lvl6pPr marL="2286000" indent="0" algn="ctr"><a:buNone/><a:defRPr><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl6pPr><a:lvl7pPr marL="2743200" indent="0" algn="ctr"><a:buNone/><a:defRPr><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl7pPr><a:lvl8pPr marL="3200400" indent="0" algn="ctr"><a:buNone/><a:defRPr><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl8pPr><a:lvl9pPr marL="3657600" indent="0" algn="ctr"><a:buNone/><a:defRPr><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl9pPr></a:lstStyle>' +
        a +
        "</p:txBody></p:sp>" +
        s +
        "</p:spTree></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr></p:sld" +
        (e.isRealSlide ? "" : "Layout") +
        ">"
    );
  }
  function o(e, t) {
    var a,
      i = "",
      r = "solid",
      s = "";
    if (
      (t &&
        ((i += "<p:bg><p:bgPr>"),
        (i += o(t, !1)),
        (i += "<a:effectLst/>"),
        (i += "</p:bgPr></p:bg>")),
      e)
    )
      switch (
        ("string" == typeof e
          ? (a = e)
          : (e.type && (r = e.type),
            e.color && (a = e.color),
            e.alpha && (s += '<a:alpha val="' + (100 - e.alpha) + '000"/>')),
        r)
      ) {
        case "solid":
          i +=
            '<a:solidFill><a:srgbClr val="' +
            a +
            '">' +
            s +
            "</a:srgbClr></a:solidFill>";
          break;
        case "gradient":
          for (var n in ((i +=
            '<a:gradFill flip="none" rotWithShape="1"><a:gsLst>'),
          a))
            i +=
              "string" == typeof a[n]
                ? '<a:gs pos="' +
                  (1e5 - Math.round((1e5 / (a.length - 1)) * n)) +
                  '"><a:srgbClr val="' +
                  a[n] +
                  '">' +
                  s +
                  "</a:srgbClr></a:gs>"
                : '<a:gs pos="' +
                  1e3 * a[n].position +
                  '"><a:srgbClr val="' +
                  a[n].color +
                  '">' +
                  s +
                  "</a:srgbClr></a:gs>";
          i +=
            void 0 !== e.angle
              ? '</a:gsLst><a:lin ang="' +
                1e5 * e.angle +
                '" scaled="1"/><a:tileRect/></a:gradFill>'
              : '</a:gsLst><a:path path="circle"><a:fillToRect l="100000" t="100000"/></a:path><a:tileRect r="-100000" b="-100000"/></a:gradFill>';
      }
    return i;
  }
  function l(e, t, a) {
    var i = a ? new Date(a) : new Date(),
      r = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ],
      s = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ],
      n = "";
    switch (e) {
      case "SLIDE_NUM":
      case "slidenum":
        n += t;
        break;
      case "DATE_TIME":
      case "datetime":
        n += i.getMonth() + 1 + "/" + i.getDate() + "/" + i.getFullYear();
        break;
      case "DATE_MM_DD_YYYY":
      case "datetime1":
        n += i.getMonth() + 1 + "/" + i.getDate() + "/" + i.getFullYear();
        break;
      case "DATE_WD_MN_DD_YYYY":
      case "datetime2":
        n +=
          [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
          ][i.getDay()] +
          ", " +
          r[i.getMonth()] +
          " " +
          i.getDate() +
          ", " +
          i.getFullYear();
        break;
      case "DATE_DD_MN_YYYY":
      case "datetime3":
        n += i.getDate() + " " + r[i.getMonth()] + " " + i.getFullYear();
        break;
      case "DATE_MN_DD_YYYY":
      case "datetime4":
        n += r[i.getMonth()] + " " + i.getDate() + ", " + i.getFullYear();
        break;
      case "DATE_DD_SMN_YY":
      case "datetime5":
        n +=
          i.getDate() + "-" + s[i.getMonth()] + "-" + (i.getFullYear() % 100);
        break;
      case "DATE_MM_YY":
      case "datetime6":
        n += r[i.getMonth()] + " " + (i.getFullYear() % 100);
        break;
      case "DATE_SMN_YY":
      case "datetime7":
        n += s[i.getMonth()] + "-" + (i.getFullYear() % 100);
        break;
      case "DATE_TIME_DD_MM_YYYY_HH_MM_PM":
      case "datetime8":
        (n += i.getMonth() + "/" + i.getDate() + "/" + i.getFullYear()),
          (n += (i.getHours() % 12) + ":" + i.getMinutes()),
          (n += i.getHours() > 11 ? " PM" : " AM");
        break;
      case "DATE_TIME_DD_MM_YYYY_HH_MM_SC_PM":
      case "datetime9":
        (n += i.getMonth() + "/" + i.getDate() + "/" + i.getFullYear()),
          (n +=
            (i.getHours() % 12) + ":" + i.getMinutes() + ":" + i.getSeconds()),
          (n += i.getHours() > 11 ? " PM" : " AM");
        break;
      case "TIME_HH_MM":
      case "datetime10":
        n += i.getHours() + ":" + i.getMinutes();
        break;
      case "TIME_HH_MM_SC":
      case "datetime11":
        n += i.getHours() + ":" + i.getMinutes() + ":" + i.getSeconds();
        break;
      case "TIME_HH_MM_PM":
      case "datetime12":
        (n += (i.getHours() % 12) + ":" + i.getMinutes()),
          (n += i.getHours() > 11 ? " PM" : " AM");
        break;
      case "TIME_HH_MM_SC_PM":
      case "datetime13":
        (n +=
          (i.getHours() % 12) + ":" + i.getMinutes() + ":" + i.getSeconds()),
          (n += i.getHours() > 11 ? " PM" : " AM");
        break;
      default:
        return null;
    }
    return n;
  }
  function p(e, t, a, r, s) {
    var n,
      p = (function (e, t) {
        var a = {
          font_size: "",
          bold: "",
          italic: "",
          strike: "",
          underline: "",
          rpr_info: "",
          char_spacing: "",
          baseline: "",
        };
        return (
          "object" === _typeof(e)
            ? (e.bold && (a.bold = ' b="1"'),
              e.italic && (a.italic = ' i="1"'),
              e.strike && (a.strike = ' strike="sngStrike"'),
              e.underline && (a.underline = ' u="sng"'),
              e.font_size && (a.font_size = ' sz="' + e.font_size + '00"'),
              e.char_spacing &&
                ((a.char_spacing = ' spc="' + 100 * e.char_spacing + '"'),
                (a.char_spacing += ' kern="0"')),
              e.baseline &&
                (a.baseline = ' baseline="' + 1e3 * e.baseline + '"'),
              e.color
                ? (a.rpr_info += o(e.color))
                : t && t.color && (a.rpr_info += o(t.color)),
              e.font_face &&
                (a.rpr_info +=
                  '<a:latin typeface="' +
                  e.font_face +
                  '" pitchFamily="34" charset="0"/><a:cs typeface="' +
                  e.font_face +
                  '" pitchFamily="34" charset="0"/>'))
            : t && t.color && (a.rpr_info += o(t.color)),
          "" !== a.rpr_info && (a.rpr_info += "</a:rPr>"),
          a
        );
      })((e = e || {}), a),
      c =
        '<a:rPr lang="en-US"' +
        [
          "font_size",
          "strike",
          "italic",
          "bold",
          "underline",
          "char_spacing",
          "baseline",
        ].reduce(function (e, t) {
          return e + p[t];
        }, "") +
        ' dirty="0" smtClean="0"' +
        ("" !== p.rpr_info ? ">" + p.rpr_info : "/>") +
        "<a:t>",
      d = "</a:r>",
      m = "<a:r>" + c;
    if (t.field) {
      d = "</a:fld>";
      var f = pptxFields[t.field];
      if (null === f) {
        for (var w in pptxFields)
          if (pptxFields[w] === t.field) {
            f = t.field;
            break;
          }
        null === f && (f = "datetime");
      }
      (m =
        '<a:fld id="{' +
        i.plugs.type.msoffice.makeUniqueID("5C7A2A3D") +
        '}" type="' +
        f +
        '">' +
        c),
        (m += l(f, r));
    } else if ((n = t.split("\n")).length > 1) {
      for (var u = "", h = 0, g = n.length; g > h; h++)
        (u += m + encodeHTML(n[h])),
          g > h + 1 && ((u += "</a:t></a:r></a:p><a:p>"), s && (u += s));
      m = u;
    } else m += encodeHTML(t);
    var y = "";
    return e.breakLine && (y += "</a:p><a:p>"), m + "</a:t>" + d + y;
  }
  function c(e, t, a, i) {
    for (var r, s, n = "", o = 0, l = t.length; l > o; o++)
      if (t[o]) {
        if (((r = ""), (s = ""), t[o].options)) {
          if (t[o].options.align)
            switch (t[o].options.align) {
              case "right":
                r += ' algn="r"';
                break;
              case "center":
                r += ' algn="ctr"';
                break;
              case "justify":
                r += ' algn="just"';
            }
          t[o].options.indentLevel > 0 &&
            (r += ' lvl="' + t[o].options.indentLevel + '"'),
            "number" === t[o].options.listType
              ? (s +=
                  '<a:buFont typeface="+mj-lt"/><a:buAutoNum type="arabicPeriod"/>')
              : "dot" === t[o].options.listType &&
                (s += '<a:buChar char="•"/>');
        }
        "" !== s
          ? (n = "<a:pPr" + r + ">" + s + "</a:pPr>")
          : "" !== r && (n = "<a:pPr" + r + "/>"),
          (!n && o) || (o && (e += "</a:p>"), (e += "<a:p>" + n)),
          (e += p(t[o].options, t[o].text, i, i.getPageNumber(), n));
      }
    var c = "";
    return (
      a && a.font_size && (c = ' sz="' + a.font_size + '00"'),
      (e += '<a:endParaRPr lang="en-US"' + c + ' dirty="0"/></a:p>')
    );
  }
  function d(e, t, a, i, r) {
    if (void 0 === e) return "number" == typeof a ? a : 0;
    "" === e && (e = 0),
      "string" != typeof e || isNaN(e) || (e = parseInt(e, 10));
    var s,
      n = Math.round(r ? e * r : e);
    if ("string" == typeof e) {
      if (-1 !== e.indexOf("%"))
        return (s = "number" == typeof t ? t : 0) > 0
          ? Math.round((s / 100) * parseInt(e, 10))
          : 0;
      if (-1 !== e.indexOf("#")) return parseInt(e, 10), s;
      var o = "number" == typeof i ? i : 0;
      if ("*" === e) return o;
      if ("c" === e) return Math.round(o / 2);
    }
    return "number" == typeof e ? n : "number" == typeof a ? a : 0;
  }
  function m(e, t) {
    var a = "<a:" + t + " ",
      i = 60,
      r = "";
    return (
      "number" == typeof e.transparency && (i = e.transparency),
      (i > 100 || 0 > i) && (i = 60),
      e.align &&
        (e.align.top && (r += "t"),
        e.align.bottom && (r += "b"),
        e.align.left && (r += "l"),
        e.align.right && (r += "r")),
      "" === r && (r = "br"),
      (a +=
        ' blurRad="50800" dist="38100" dir="13500000" algn="' +
        r +
        '" rotWithShape="0"'),
      (a +=
        '><a:prstClr val="' +
        (e.color || "black") +
        '"><a:alpha val="' +
        (i = 1e3 * (100 - i)) +
        '"/></a:prstClr>') +
        "</a:" +
        t +
        ">"
    );
  }
  function f(e) {
    var t = "<a:bodyPr";
    return (
      e && e.bodyProp
        ? (e.bodyProp.anchor && (t += ' anchor="' + e.bodyProp.anchor + '"'),
          e.bodyProp.anchorCtr &&
            (t += ' anchorCtr="' + e.bodyProp.anchorCtr + '"'),
          (t += e.bodyProp.wrap
            ? ' wrap="' + e.bodyProp.wrap + '"'
            : ' wrap="square"'),
          e.bodyProp.bIns &&
            (t +=
              ' bIns="' +
              d(
                e.bodyProp.bIns,
                i.type.pptx.pptHeight,
                369332,
                i.type.pptx.pptHeight,
                1e4
              ) +
              '"'),
          e.bodyProp.lIns &&
            (t +=
              ' lIns="' +
              d(
                e.bodyProp.lIns,
                i.type.pptx.pptWidth,
                2819400,
                i.type.pptx.pptWidth,
                1e4
              ) +
              '"'),
          e.bodyProp.rIns &&
            (t +=
              ' rIns="' +
              d(
                e.bodyProp.rIns,
                i.type.pptx.pptWidth,
                2819400,
                i.type.pptx.pptWidth,
                1e4
              ) +
              '"'),
          e.bodyProp.tIns &&
            (t +=
              ' tIns="' +
              d(
                e.bodyProp.tIns,
                i.type.pptx.pptHeight,
                369332,
                i.type.pptx.pptHeight,
                1e4
              ) +
              '"'),
          (t += ' rtlCol="0">'),
          !1 !== e.bodyProp.autoFit && (t += "<a:spAutoFit/>"),
          (t += "</a:bodyPr>"))
        : (t += ' wrap="square" rtlCol="0"></a:bodyPr>'),
      t
    );
  }
  function w(e, t) {
    var a = "",
      r = e.data;
    if (
      e.slide.useLayout &&
      "object" === _typeof(e.slide.useLayout) &&
      e.slide.useLayout.mkResCb
    )
      return (
        (e.slide.useLayout.slide = e.slide),
        e.slide.useLayout.mkResCb(e.slide.useLayout)
      );
    var n = e.slide.layoutName ? "Layout" : "",
      l = e.slide.layoutName
        ? ' type="' + e.slide.layoutName + '" preserve="1"'
        : "";
    t ||
      ((a =
        i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
        "<p:sld" +
        n +
        ' xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"'),
      e.slide.show || e.slide.layoutName || (a += ' show="0"'),
      (a +=
        l +
        "><p:cSld" +
        (e.slide.name ? ' name="' + e.slide.name + '"' : "") +
        ">"),
      e.slide.back && (a += o(!1, e.slide.back)),
      (a +=
        '<p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr>'));
    for (var w = 0, u = r.length; u > w; w++) {
      var h = 0,
        g = 0,
        y = 2819400,
        x = 369332,
        P = "",
        v = "",
        b = null,
        S = "";
      if (
        r[w].options &&
        (void 0 !== r[w].options.cx &&
          (y = r[w].options.cx
            ? d(
                r[w].options.cx,
                i.type.pptx.pptWidth,
                y,
                i.type.pptx.pptWidth,
                1e4
              )
            : 1),
        void 0 !== r[w].options.cy &&
          (x = r[w].options.cy
            ? d(
                r[w].options.cy,
                i.type.pptx.pptHeight,
                x,
                i.type.pptx.pptHeight,
                1e4
              )
            : 1),
        r[w].options.x &&
          (h = d(
            r[w].options.x,
            i.type.pptx.pptWidth,
            0,
            i.type.pptx.pptWidth - y,
            1e4
          )),
        r[w].options.y &&
          (g = d(
            r[w].options.y,
            i.type.pptx.pptHeight,
            0,
            i.type.pptx.pptHeight - x,
            1e4
          )),
        r[w].options.shape && (b = s(r[w].options.shape)),
        r[w].options.flip_vertical && (S += ' flipV="1"'),
        r[w].options.flip_horizontal && (S += ' flipH="1"'),
        r[w].options.rotate)
      ) {
        var k =
          r[w].options.rotate > 360
            ? r[w].options.rotate - 360
            : r[w].options.rotate;
        S += ' rot="' + (k *= 6e4) + '"';
      }
      switch (r[w].type) {
        case "table":
          var _ = officeTable.getTable(r[w].data, r[w].options);
          a += xmlBuilder
            .create(_, { version: "1.0", encoding: "UTF-8", standalone: !0 })
            .toString({ pretty: !0, indent: "  ", newline: "\n" });
          break;
        case "chart":
          "pie" === r[w].renderType
            ? (a +=
                '<p:graphicFrame><p:nvGraphicFramePr><p:cNvPr id="20" name="OfficeChart 19"/><p:cNvGraphicFramePr/><p:nvPr><p:extLst><p:ext uri="{D42A27DB-BD31-4B8C-83A1-F6EECF244321}"><p14:modId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="4198609065"/></p:ext></p:extLst></p:nvPr></p:nvGraphicFramePr><p:xfrm><a:off x="' +
                (r[w].options.x || 1524e3) +
                '" y="' +
                (r[w].options.y || 1397e3) +
                '"/><a:ext cx="' +
                (r[w].options.cx || 6096e3) +
                '" cy="' +
                (r[w].options.cy || 4064e3) +
                '"/></p:xfrm><a:graphic><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/chart"><c:chart xmlns:c="http://schemas.openxmlformats.org/drawingml/2006/chart" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" r:id="rId2"/></a:graphicData></a:graphic></p:graphicFrame>')
            : "column" === r[w].renderType &&
              (a +=
                '<p:graphicFrame><p:nvGraphicFramePr><p:cNvPr id="4" name="OfficeChart 3"/><p:cNvGraphicFramePr/><p:nvPr><p:extLst><p:ext uri="{D42A27DB-BD31-4B8C-83A1-F6EECF244321}"><p14:modId xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="1256887135"/></p:ext></p:extLst></p:nvPr></p:nvGraphicFramePr><p:xfrm><a:off x="' +
                (r[w].options.x || 1524e3) +
                '" y="' +
                (r[w].options.y || 1397e3) +
                '"/><a:ext cx="' +
                (r[w].options.cx || 6096e3) +
                '" cy="' +
                (r[w].options.cy || 4064e3) +
                '"/></p:xfrm><a:graphic><a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/chart"><c:chart xmlns:c="http://schemas.openxmlformats.org/drawingml/2006/chart" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" r:id="rId2"/></a:graphicData></a:graphic></p:graphicFrame>');
          break;
        case "text":
        case "cxn":
          var L = "";
          if (
            (null == b && (b = s(null)),
            "cxn" === r[w].type
              ? ((a += "<p:cxnSp><p:nvCxnSpPr>"),
                (a +=
                  '<p:cNvPr id="' +
                  (w + 2) +
                  '" name="Object ' +
                  (w + 1) +
                  '"/><p:nvPr/></p:nvCxnSpPr>'))
              : ((a += "<p:sp><p:nvSpPr>"),
                (a +=
                  '<p:cNvPr id="' +
                  (w + 2) +
                  '" name="Object ' +
                  (w + 1) +
                  '"/><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr>')),
            (a += "<p:spPr>"),
            (a += "<a:xfrm" + S + ">"),
            (a +=
              '<a:off x="' +
              h +
              '" y="' +
              g +
              '"/><a:ext cx="' +
              y +
              '" cy="' +
              x +
              '"/></a:xfrm><a:prstGeom prst="' +
              b.name +
              '">'),
            b.avLst !== {})
          )
            for (var A in ((a += "<a:avLst>"), b.avLst))
              a += '<a:gd name="' + A + '" fmla="val ' + b.avLst[A] + '"/>';
          if (((a += "</a:avLst></a:prstGeom>"), r[w].options)) {
            if (
              ((a += r[w].options.fill ? o(r[w].options.fill) : "<a:noFill/>"),
              r[w].options.line)
            ) {
              var E = "";
              r[w].options.line_size &&
                (E += ' w="' + 12700 * r[w].options.line_size + '"'),
                (a += "<a:ln" + E + ">"),
                (a += o(r[w].options.line)),
                r[w].options.line_head &&
                  (a += '<a:headEnd type="' + r[w].options.line_head + '"/>'),
                r[w].options.line_tail &&
                  (a += '<a:tailEnd type="' + r[w].options.line_tail + '"/>'),
                (a += "</a:ln>");
            }
          } else a += "<a:noFill/>";
          if (r[w].options.effects)
            for (var T = 0, F = r[w].options.effects.length; F > T; T++)
              switch (r[w].options.effects[T].type) {
                case "outerShadow":
                  L += m(r[w].options.effects[T], "outerShdw");
                  break;
                case "innerShadow":
                  L += m(r[w].options.effects[T], "innerShdw");
              }
          if (
            ("" !== L && (a += "<a:effectLst>" + L + "</a:effectLst>"),
            (a += "</p:spPr>"),
            r[w].options)
          ) {
            if (r[w].options.align)
              switch (r[w].options.align) {
                case "right":
                  P += ' algn="r"';
                  break;
                case "center":
                  P += ' algn="ctr"';
                  break;
                case "justify":
                  P += ' algn="just"';
              }
            r[w].options.indentLevel > 0 &&
              (P += ' lvl="' + r[w].options.indentLevel + '"');
          }
          if (
            ("" !== P && (v = "<a:pPr" + P + "/>"),
            "string" == typeof r[w].text)
          )
            (a += "<p:txBody>" + f(r[w].options) + "<a:lstStyle/><a:p>" + v),
              (a += p(
                r[w].options,
                r[w].text,
                e.slide,
                e.slide.getPageNumber(),
                v
              ));
          else if ("number" == typeof r[w].text)
            (a += "<p:txBody>" + f(r[w].options) + "<a:lstStyle/><a:p>" + v),
              (a += p(
                r[w].options,
                r[w].text + "",
                e.slide,
                e.slide.getPageNumber(),
                v
              ));
          else if (r[w].text && r[w].text.length) {
            var M = f(r[w].options);
            a += "<p:txBody>" + M + "<a:lstStyle/><a:p>" + v;
            for (var C = 0, D = r[w].text.length; D > C; C++)
              "object" === _typeof(r[w].text[C]) && r[w].text[C].text
                ? (a += p(
                    r[w].text[C].options || r[w].options,
                    r[w].text[C].text,
                    e.slide,
                    M,
                    v,
                    e.slide.getPageNumber()
                  ))
                : "string" == typeof r[w].text[C]
                ? (a += p(
                    r[w].options,
                    r[w].text[C],
                    e.slide,
                    M,
                    v,
                    e.slide.getPageNumber()
                  ))
                : "number" == typeof r[w].text[C]
                ? (a += p(
                    r[w].options,
                    r[w].text[C] + "",
                    e.slide,
                    M,
                    v,
                    e.slide.getPageNumber()
                  ))
                : "object" === _typeof(r[w].text[C]) &&
                  r[w].text[C].field &&
                  (a += p(
                    r[w].options,
                    r[w].text[C],
                    e.slide,
                    M,
                    v,
                    e.slide.getPageNumber()
                  ));
          } else
            "object" === _typeof(r[w].text) &&
              (r[w].text || (r[w].text = {}),
              r[w].text.field || (r[w].text.field = ""),
              (a += "<p:txBody>" + f(r[w].options) + "<a:lstStyle/><a:p>" + v),
              (a += p(
                r[w].options,
                r[w].text,
                e.slide,
                e.slide.getPageNumber(),
                v
              )));
          if (void 0 !== r[w].text) {
            var H = "";
            r[w].options &&
              r[w].options.font_size &&
              (H = ' sz="' + r[w].options.font_size + '00"'),
              (a +=
                '<a:endParaRPr lang="en-US"' +
                H +
                ' dirty="0"/></a:p></p:txBody>');
          }
          a += "cxn" === r[w].type ? "</p:cxnSp>" : "</p:sp>";
          break;
        case "image":
          var R = [];
          R.push(
            '<p:pic><p:nvPicPr><p:cNvPr id="' +
              (w + 2) +
              '" name="Object ' +
              (w + 1) +
              '"'
          ),
            R.push(
              r[w].link_rel_id
                ? '><a:hlinkClick r:id="rId' +
                    r[w].link_rel_id +
                    '"/></p:cNvPr>'
                : "/>"
            ),
            R.push(
              '<p:cNvPicPr><a:picLocks noChangeAspect="1"/></p:cNvPicPr><p:nvPr/></p:nvPicPr><p:blipFill><a:blip r:embed="rId' +
                r[w].rel_id +
                '" cstate="print"/><a:stretch><a:fillRect/></a:stretch></p:blipFill><p:spPr><a:xfrm' +
                S +
                '><a:off x="' +
                h +
                '" y="' +
                g +
                '"/><a:ext cx="' +
                y +
                '" cy="' +
                x +
                '"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></p:spPr></p:pic>'
            ),
            (a += R.join(""));
          break;
        case "p":
          null == b && (b = s(null)),
            (a += "<p:sp><p:nvSpPr>"),
            (a +=
              '<p:cNvPr id="' +
              (w + 2) +
              '" name="Object ' +
              (w + 1) +
              '"/><p:cNvSpPr txBox="1"/><p:nvPr/></p:nvSpPr>'),
            (a += "<p:spPr>"),
            (a += "<a:xfrm" + S + ">"),
            (a +=
              '<a:off x="' +
              h +
              '" y="' +
              g +
              '"/><a:ext cx="' +
              y +
              '" cy="' +
              x +
              '"/></a:xfrm><a:prstGeom prst="' +
              b.name +
              '"><a:avLst/></a:prstGeom>'),
            r[w].options
              ? ((a += r[w].options.fill
                  ? o(r[w].options.fill)
                  : "<a:noFill/>"),
                r[w].options.line &&
                  ((a += "<a:ln>"),
                  (a += o(r[w].options.line)),
                  r[w].options.line_head &&
                    (a += '<a:headEnd type="' + r[w].options.line_head + '"/>'),
                  r[w].options.line_tail &&
                    (a += '<a:tailEnd type="' + r[w].options.line_tail + '"/>'),
                  (a += "</a:ln>")))
              : (a += "<a:noFill/>"),
            (a += "</p:spPr>"),
            (a = c(
              (a +=
                '<p:txBody><a:bodyPr wrap="square" rtlCol="0"><a:spAutoFit/></a:bodyPr><a:lstStyle/>'),
              r[w].data,
              r[w].options,
              e.slide
            )),
            (a += "</p:txBody>"),
            (a += "</p:sp>");
      }
    }
    return (
      t ||
        ((a +=
          "</p:spTree></p:cSld><p:clrMapOvr><a:masterClrMapping/></p:clrMapOvr>"),
        (a += "</p:sld" + n + ">")),
      a
    );
  }
  function u(e) {
    return new OfficeChart(e).toXML();
  }
  function h(e) {}
  (e.shapes = pptxShapes),
    (e.fields = pptxFields),
    (e.options = a && "object" === _typeof(a) ? a : {}),
    e.options.tempDir || (e.options.tempDir = "./"),
    msdoc.makemsdoc(e, t, a, i, r),
    i.plugs.type.msoffice.makeOfficeGenerator("ppt", "presentation", {}),
    (i.features.page_name = "slides"),
    i.plugs.type.msoffice.addInfoType("dc:title", "", "title", "setDocTitle"),
    e.on("beforeGen", function () {
      (e.generate_data = {}),
        i.features.type.pptx.emitEvent("beforeGen", e),
        i.features.type.pptx.emitEvent("beforeGenFinal", e),
        i.type.msoffice.rels_app.push(
          {
            type:
              "http://schemas.openxmlformats.org/officeDocument/2006/relationships/presProps",
            target: "presProps.xml",
            clear: "type",
          },
          {
            type:
              "http://schemas.openxmlformats.org/officeDocument/2006/relationships/viewProps",
            target: "viewProps.xml",
            clear: "type",
          },
          {
            type:
              "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme",
            target: "theme/theme1.xml",
            clear: "type",
          }
        );
      var t = [];
      i.features.type.pptx.emitEvent("addMainRels", { genobj: e, relsList: t }),
        t.forEach(function (e) {
          i.type.msoffice.rels_app.push(e);
        }),
        i.type.pptx.extraMainRelList &&
          "object" === _typeof(i.type.pptx.extraMainRelList) &&
          i.type.pptx.extraMainRelList.forEach &&
          i.type.pptx.extraMainRelList.forEach(function (e) {
            i.type.msoffice.rels_app.push(e);
          }),
        i.type.msoffice.rels_app.push({
          type:
            "http://schemas.openxmlformats.org/officeDocument/2006/relationships/tableStyles",
          target: "tableStyles.xml",
          clear: "type",
        });
    });
  var g = "slideshow";
  "ppsx" !== t && (g = "presentation");
  var y = new docplugman(e, i, "pptx", function (e) {
    var t = e.getDataStorage();
    (t.EMUS_PER_PT = 12700),
      (t.pptWidth = 720 * t.EMUS_PER_PT),
      (t.pptHeight = 540 * t.EMUS_PER_PT),
      (t.pptType = "screen4x3"),
      (t.extraMainRelList = []);
  });
  return (
    y.plugsList.push(new plugWidescreen(y)),
    y.plugsList.push(new plugSpeakernotes(y)),
    y.plugsList.push(new plugLayouts(y)),
    a.extraPlugs &&
      "object" === _typeof(a.extraPlugs) &&
      a.extraPlugs.forEach &&
      a.extraPlugs.forEach(function (e) {
        var t;
        e &&
          ("function" == typeof e
            ? (t = e)
            : "string" == typeof e && (t = require("./" + e))),
          y.plugsList.push(new t(y));
      }),
    (e.cbMakePptxLayout1 = n),
    (e.cbMakePptxSlide = w),
    (e.createFieldText = l),
    (e.cMakePptxOutTextP = c),
    i.type.msoffice.files_list.push(
      {
        name: "/ppt/slideMasters/slideMaster1.xml",
        type:
          "application/vnd.openxmlformats-officedocument.presentationml.slideMaster+xml",
        clear: "type",
      },
      {
        name: "/ppt/presProps.xml",
        type:
          "application/vnd.openxmlformats-officedocument.presentationml.presProps+xml",
        clear: "type",
      },
      {
        name: "/ppt/presentation.xml",
        type:
          "application/vnd.openxmlformats-officedocument.presentationml." +
          g +
          ".main+xml",
        clear: "type",
      },
      {
        name: "/ppt/slideLayouts/slideLayout1.xml",
        type:
          "application/vnd.openxmlformats-officedocument.presentationml.slideLayout+xml",
        clear: "type",
      },
      {
        name: "/ppt/tableStyles.xml",
        type:
          "application/vnd.openxmlformats-officedocument.presentationml.tableStyles+xml",
        clear: "type",
      },
      {
        name: "/ppt/viewProps.xml",
        type:
          "application/vnd.openxmlformats-officedocument.presentationml.viewProps+xml",
        clear: "type",
      }
    ),
    (e.slideMasterRels = [
      {
        type:
          "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout",
        target: "../slideLayouts/slideLayout1.xml",
      },
      {
        type:
          "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme",
        target: "../theme/theme1.xml",
      },
    ]),
    i.plugs.intAddAnyResourceToParse(
      "ppt\\presProps.xml",
      "buffer",
      null,
      function (e) {
        return (
          i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
          '<p:presentationPr xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:extLst><p:ext uri="{E76CE94A-603C-4142-B9EB-6D1370010A27}"><p14:discardImageEditData xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="0"/></p:ext><p:ext uri="{D31A062A-798A-4329-ABDD-BBA856620510}"><p14:defaultImageDpi xmlns:p14="http://schemas.microsoft.com/office/powerpoint/2010/main" val="220"/></p:ext><p:ext uri="{FD5EFAAD-0ECE-453E-9831-46B23BE46B34}"><p15:chartTrackingRefBased xmlns:p15="http://schemas.microsoft.com/office/powerpoint/2012/main" val="1"/></p:ext></p:extLst></p:presentationPr>'
        );
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "ppt\\tableStyles.xml",
      "buffer",
      null,
      function (e) {
        return (
          i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
          '<a:tblStyleLst xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" def="{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}"/>'
        );
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "ppt\\viewProps.xml",
      "buffer",
      null,
      function (e) {
        return (
          i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
          '<p:viewPr xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:normalViewPr><p:restoredLeft sz="15620"/><p:restoredTop sz="94660"/></p:normalViewPr><p:slideViewPr><p:cSldViewPr><p:cViewPr varScale="1"><p:scale><a:sx n="64" d="100"/><a:sy n="64" d="100"/></p:scale><p:origin x="-1392" y="-96"/></p:cViewPr><p:guideLst><p:guide orient="horz" pos="2160"/><p:guide pos="2880"/></p:guideLst></p:cSldViewPr></p:slideViewPr><p:notesTextViewPr><p:cViewPr><p:scale><a:sx n="100" d="100"/><a:sy n="100" d="100"/></p:scale><p:origin x="0" y="0"/></p:cViewPr></p:notesTextViewPr><p:gridSpacing cx="78028800" cy="78028800"/></p:viewPr>'
        );
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "ppt\\presentation.xml",
      "buffer",
      null,
      function (e) {
        var t =
            i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
            '<p:presentation xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main" saveSubsetFonts="1"><p:sldMasterIdLst><p:sldMasterId id="2147483648" r:id="rId1"/></p:sldMasterIdLst>',
          a = { data: t };
        i.features.type.pptx.emitEvent("presentationGen", a),
          (t = a.data + "<p:sldIdLst>");
        for (var r = 0, s = i.pages.length; s > r; r++)
          t += '<p:sldId id="' + (r + 256) + '" r:id="rId' + (r + 2) + '"/>';
        t +=
          '</p:sldIdLst><p:sldSz cx="' +
          i.type.pptx.pptWidth +
          '" cy="' +
          i.type.pptx.pptHeight +
          '" type="' +
          i.type.pptx.pptType +
          '"/><p:notesSz cx="' +
          i.type.pptx.pptHeight +
          '" cy="' +
          i.type.pptx.pptWidth +
          '"/><p:defaultTextStyle><a:defPPr><a:defRPr lang="en-US"/></a:defPPr>';
        var n = 0;
        for (r = 1; 10 > r; r++)
          (t +=
            "<a:lvl" +
            r +
            'pPr marL="' +
            n +
            '" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl' +
            r +
            "pPr>"),
            (n += 457200);
        return (
          (t += "</p:defaultTextStyle>"),
          (t +=
            '<p:extLst><p:ext uri="{EFAFB233-063F-42B5-8137-9DF3F51BA10A}"><p15:sldGuideLst xmlns:p15="http://schemas.microsoft.com/office/powerpoint/2012/main"/></p:ext></p:extLst></p:presentation>')
        );
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "ppt\\slideLayouts\\slideLayout1.xml",
      "buffer",
      null,
      n,
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "ppt\\slideLayouts\\_rels\\slideLayout1.xml.rels",
      "buffer",
      [
        {
          type:
            "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideMaster",
          target: "../slideMasters/slideMaster1.xml",
        },
      ],
      i.plugs.type.msoffice.cbMakeRels,
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "ppt\\slideMasters\\slideMaster1.xml",
      "buffer",
      null,
      function (e) {
        var t =
            i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
            '<p:sldMaster xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:p="http://schemas.openxmlformats.org/presentationml/2006/main"><p:cSld><p:bg><p:bgRef idx="1001"><a:schemeClr val="bg1"/></p:bgRef></p:bg><p:spTree><p:nvGrpSpPr><p:cNvPr id="1" name=""/><p:cNvGrpSpPr/><p:nvPr/></p:nvGrpSpPr><p:grpSpPr><a:xfrm><a:off x="0" y="0"/><a:ext cx="0" cy="0"/><a:chOff x="0" y="0"/><a:chExt cx="0" cy="0"/></a:xfrm></p:grpSpPr><p:sp><p:nvSpPr><p:cNvPr id="2" name="Title Placeholder 1"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="title"/></p:nvPr></p:nvSpPr><p:spPr><a:xfrm><a:off x="457200" y="274638"/><a:ext cx="8229600" cy="1143000"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></p:spPr><p:txBody><a:bodyPr vert="horz" lIns="91440" tIns="45720" rIns="91440" bIns="45720" rtlCol="0" anchor="ctr"><a:normAutofit/></a:bodyPr><a:lstStyle/><a:p><a:r><a:rPr lang="en-US" smtClean="0"/><a:t>Click to edit Master title style</a:t></a:r><a:endParaRPr lang="en-US"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="3" name="Text Placeholder 2"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="body" idx="1"/></p:nvPr></p:nvSpPr><p:spPr><a:xfrm><a:off x="457200" y="1600200"/><a:ext cx="8229600" cy="4525963"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></p:spPr><p:txBody><a:bodyPr vert="horz" lIns="91440" tIns="45720" rIns="91440" bIns="45720" rtlCol="0"><a:normAutofit/></a:bodyPr><a:lstStyle/><a:p><a:pPr lvl="0"/><a:r><a:rPr lang="en-US" smtClean="0"/><a:t>Click to edit Master text styles</a:t></a:r></a:p><a:p><a:pPr lvl="1"/><a:r><a:rPr lang="en-US" smtClean="0"/><a:t>Second level</a:t></a:r></a:p><a:p><a:pPr lvl="2"/><a:r><a:rPr lang="en-US" smtClean="0"/><a:t>Third level</a:t></a:r></a:p><a:p><a:pPr lvl="3"/><a:r><a:rPr lang="en-US" smtClean="0"/><a:t>Fourth level</a:t></a:r></a:p><a:p><a:pPr lvl="4"/><a:r><a:rPr lang="en-US" smtClean="0"/><a:t>Fifth level</a:t></a:r><a:endParaRPr lang="en-US"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="4" name="Date Placeholder 3"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="dt" sz="half" idx="2"/></p:nvPr></p:nvSpPr><p:spPr><a:xfrm><a:off x="457200" y="6356350"/><a:ext cx="2133600" cy="365125"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></p:spPr><p:txBody><a:bodyPr vert="horz" lIns="91440" tIns="45720" rIns="91440" bIns="45720" rtlCol="0" anchor="ctr"/><a:lstStyle><a:lvl1pPr algn="l"><a:defRPr sz="1200"><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl1pPr></a:lstStyle><a:p><a:fld id="{F8166F1F-CE9B-4651-A6AA-CD717754106B}" type="datetimeFigureOut"><a:rPr lang="en-US" smtClean="0"/><a:t>6/13/2013</a:t></a:fld><a:endParaRPr lang="en-US"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="5" name="Footer Placeholder 4"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="ftr" sz="quarter" idx="3"/></p:nvPr></p:nvSpPr><p:spPr><a:xfrm><a:off x="3124200" y="6356350"/><a:ext cx="2895600" cy="365125"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></p:spPr><p:txBody><a:bodyPr vert="horz" lIns="91440" tIns="45720" rIns="91440" bIns="45720" rtlCol="0" anchor="ctr"/><a:lstStyle><a:lvl1pPr algn="ctr"><a:defRPr sz="1200"><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl1pPr></a:lstStyle><a:p><a:endParaRPr lang="en-US"/></a:p></p:txBody></p:sp><p:sp><p:nvSpPr><p:cNvPr id="6" name="Slide Number Placeholder 5"/><p:cNvSpPr><a:spLocks noGrp="1"/></p:cNvSpPr><p:nvPr><p:ph type="sldNum" sz="quarter" idx="4"/></p:nvPr></p:nvSpPr><p:spPr><a:xfrm><a:off x="6553200" y="6356350"/><a:ext cx="2133600" cy="365125"/></a:xfrm><a:prstGeom prst="rect"><a:avLst/></a:prstGeom></p:spPr><p:txBody><a:bodyPr vert="horz" lIns="91440" tIns="45720" rIns="91440" bIns="45720" rtlCol="0" anchor="ctr"/><a:lstStyle><a:lvl1pPr algn="r"><a:defRPr sz="1200"><a:solidFill><a:schemeClr val="tx1"><a:tint val="75000"/></a:schemeClr></a:solidFill></a:defRPr></a:lvl1pPr></a:lstStyle><a:p><a:fld id="{F7021451-1387-4CA6-816F-3879F97B5CBC}" type="slidenum"><a:rPr lang="en-US" smtClean="0"/><a:t>�#�</a:t></a:fld><a:endParaRPr lang="en-US"/></a:p></p:txBody></p:sp></p:spTree></p:cSld><p:clrMap bg1="lt1" tx1="dk1" bg2="lt2" tx2="dk2" accent1="accent1" accent2="accent2" accent3="accent3" accent4="accent4" accent5="accent5" accent6="accent6" hlink="hlink" folHlink="folHlink"/><p:sldLayoutIdLst>',
          a = 1,
          r = 2147483649,
          s = y.getDataStorage();
        if (
          ((t += '<p:sldLayoutId id="' + r + '" r:id="rId' + a + '"/>'),
          a++,
          r++,
          s.slideLayouts && "object" === _typeof(s.slideLayouts))
        )
          for (var n in s.slideLayouts)
            s.slideLayouts[n] &&
              ((t +=
                '<p:sldLayoutId id="' +
                r +
                '" r:id="rId' +
                s.slideLayouts[n].relIdMaster +
                '"/>'),
              a++,
              r++);
        return (t +=
          '</p:sldLayoutIdLst><p:txStyles><p:titleStyle><a:lvl1pPr algn="ctr" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:spcBef><a:spcPct val="0"/></a:spcBef><a:buNone/><a:defRPr sz="4400" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mj-lt"/><a:ea typeface="+mj-ea"/><a:cs typeface="+mj-cs"/></a:defRPr></a:lvl1pPr></p:titleStyle><p:bodyStyle><a:lvl1pPr marL="342900" indent="-342900" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:spcBef><a:spcPct val="20000"/></a:spcBef><a:buFont typeface="Arial" pitchFamily="34" charset="0"/><a:buChar char="�"/><a:defRPr sz="3200" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl1pPr><a:lvl2pPr marL="742950" indent="-285750" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:spcBef><a:spcPct val="20000"/></a:spcBef><a:buFont typeface="Arial" pitchFamily="34" charset="0"/><a:buChar char="�"/><a:defRPr sz="2800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl2pPr><a:lvl3pPr marL="1143000" indent="-228600" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:spcBef><a:spcPct val="20000"/></a:spcBef><a:buFont typeface="Arial" pitchFamily="34" charset="0"/><a:buChar char="�"/><a:defRPr sz="2400" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl3pPr><a:lvl4pPr marL="1600200" indent="-228600" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:spcBef><a:spcPct val="20000"/></a:spcBef><a:buFont typeface="Arial" pitchFamily="34" charset="0"/><a:buChar char="�"/><a:defRPr sz="2000" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl4pPr><a:lvl5pPr marL="2057400" indent="-228600" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:spcBef><a:spcPct val="20000"/></a:spcBef><a:buFont typeface="Arial" pitchFamily="34" charset="0"/><a:buChar char="�"/><a:defRPr sz="2000" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl5pPr><a:lvl6pPr marL="2514600" indent="-228600" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:spcBef><a:spcPct val="20000"/></a:spcBef><a:buFont typeface="Arial" pitchFamily="34" charset="0"/><a:buChar char="�"/><a:defRPr sz="2000" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl6pPr><a:lvl7pPr marL="2971800" indent="-228600" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:spcBef><a:spcPct val="20000"/></a:spcBef><a:buFont typeface="Arial" pitchFamily="34" charset="0"/><a:buChar char="�"/><a:defRPr sz="2000" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl7pPr><a:lvl8pPr marL="3429000" indent="-228600" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:spcBef><a:spcPct val="20000"/></a:spcBef><a:buFont typeface="Arial" pitchFamily="34" charset="0"/><a:buChar char="�"/><a:defRPr sz="2000" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl8pPr><a:lvl9pPr marL="3886200" indent="-228600" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:spcBef><a:spcPct val="20000"/></a:spcBef><a:buFont typeface="Arial" pitchFamily="34" charset="0"/><a:buChar char="�"/><a:defRPr sz="2000" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl9pPr></p:bodyStyle><p:otherStyle><a:defPPr><a:defRPr lang="en-US"/></a:defPPr><a:lvl1pPr marL="0" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl1pPr><a:lvl2pPr marL="457200" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl2pPr><a:lvl3pPr marL="914400" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl3pPr><a:lvl4pPr marL="1371600" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl4pPr><a:lvl5pPr marL="1828800" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl5pPr><a:lvl6pPr marL="2286000" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl6pPr><a:lvl7pPr marL="2743200" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl7pPr><a:lvl8pPr marL="3200400" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl8pPr><a:lvl9pPr marL="3657600" algn="l" defTabSz="914400" rtl="0" eaLnBrk="1" latinLnBrk="0" hangingPunct="1"><a:defRPr sz="1800" kern="1200"><a:solidFill><a:schemeClr val="tx1"/></a:solidFill><a:latin typeface="+mn-lt"/><a:ea typeface="+mn-ea"/><a:cs typeface="+mn-cs"/></a:defRPr></a:lvl9pPr></p:otherStyle></p:txStyles></p:sldMaster>');
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "ppt\\slideMasters\\_rels\\slideMaster1.xml.rels",
      "buffer",
      e.slideMasterRels,
      i.plugs.type.msoffice.cbMakeRels,
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "docProps\\app.xml",
      "buffer",
      null,
      function (t) {
        for (
          var a = i.pages.length,
            r = e.options.author || e.options.creator || "officegen",
            s =
              i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(t) +
              '<Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes"><TotalTime>0</TotalTime><Words>0</Words><Application>Microsoft Office PowerPoint</Application><PresentationFormat>On-screen Show (4:3)</PresentationFormat><Paragraphs>0</Paragraphs><Slides>' +
              a +
              '</Slides><Notes>0</Notes><HiddenSlides>0</HiddenSlides><MMClips>0</MMClips><ScaleCrop>false</ScaleCrop><HeadingPairs><vt:vector size="4" baseType="variant"><vt:variant><vt:lpstr>Theme</vt:lpstr></vt:variant><vt:variant><vt:i4>1</vt:i4></vt:variant><vt:variant><vt:lpstr>Slide Titles</vt:lpstr></vt:variant><vt:variant><vt:i4>' +
              a +
              '</vt:i4></vt:variant></vt:vector></HeadingPairs><TitlesOfParts><vt:vector size="' +
              (a + 1) +
              '" baseType="lpstr"><vt:lpstr>Office Theme</vt:lpstr>',
            n = 0,
            o = i.pages.length;
          o > n;
          n++
        )
          s += "<vt:lpstr>" + encodeHTML(i.pages[n].slide.name) + "</vt:lpstr>";
        return (s +=
          "</vt:vector></TitlesOfParts><Company>" +
          r +
          "</Company><LinksUpToDate>false</LinksUpToDate><SharedDoc>false</SharedDoc><HyperlinksChanged>false</HyperlinksChanged><AppVersion>12.0000</AppVersion></Properties>");
      },
      !0
    ),
    i.type.msoffice.rels_app.push({
      type:
        "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideMaster",
      target: "slideMasters/slideMaster1.xml",
      clear: "type",
    }),
    i.plugs.intAddAnyResourceToParse(
      "ppt\\_rels\\presentation.xml.rels",
      "buffer",
      i.type.msoffice.rels_app,
      i.plugs.type.msoffice.cbMakeRels,
      !0
    ),
    (e.makeNewSlide = function (t) {
      var a = i.pages.length,
        r = { show: !0 };
      function s(e) {
        e.api || (e.api = {}), (e.api.options = e.options);
      }
      function n(e) {
        e.api || (e.api = {}),
          (e.api.setShadowEffect = function (t, a, i, r, s, n, o, l) {
            e.options.effects || (e.options.effects = []),
              e.options.effects.push({
                type: t,
                align: a,
                color: i,
                transparency: r,
                size: s,
                blur: n,
                angle: o,
                distance: l,
              });
          });
      }
      return (
        (t && "object" === _typeof(t)) || (t = {}),
        t.basedLayout || (t.basedLayout = 1),
        (i.pages[a] = {}),
        (i.pages[a].slide = r),
        (i.pages[a].data = []),
        (i.pages[a].rels = [
          {
            type:
              "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slideLayout",
            target: "../slideLayouts/slideLayout" + t.basedLayout + ".xml",
            clear: "data",
          },
        ]),
        i.type.msoffice.files_list.push({
          name: "/ppt/slides/slide" + (a + 1) + ".xml",
          type:
            "application/vnd.openxmlformats-officedocument.presentationml.slide+xml",
          clear: "data",
        }),
        i.type.msoffice.rels_app.push({
          type:
            "http://schemas.openxmlformats.org/officeDocument/2006/relationships/slide",
          target: "slides/slide" + (a + 1) + ".xml",
          clear: "data",
        }),
        (r.getPageNumber = function () {
          return a;
        }),
        (r.getRelFile = function () {
          return i.pages[a].rels;
        }),
        (r.name = "Slide " + (a + 1)),
        (r.createChart = function (e) {
          return new OfficeChart(e);
        }),
        (r.addTable = function (e, t) {
          var r = i.pages[a].data.length;
          return (
            (i.pages[a].data[r] = { type: "table", data: e, options: t || {} }),
            s(i.pages[a].data[r]),
            n(i.pages[a].data[r]),
            i.pages[a].data[r].api
          );
        }),
        (r.addChart = function (t, r, s) {
          var n = i.pages[a].data.length;
          (GLOBAL_CHART_COUNT += 1),
            (i.pages[a].data[n] = {}),
            (i.pages[a].data[n].type = "chart"),
            (i.pages[a].data[n].renderType = "column"),
            (i.pages[a].data[n].title = t.title),
            (i.pages[a].data[n].data = t.data),
            (i.pages[a].data[n].options = t);
          for (
            var o = excelbuilder.createWorkbook(
                e.options.tempDir,
                "sample" + GLOBAL_CHART_COUNT + ".xlsx"
              ),
              l = o.createSheet(
                "Sheet1",
                t.data.length + 1,
                t.data[0].values.length + 1
              ),
              p = 0;
            t.data.length > p;
            p++
          )
            l.set(p + 2, 1, t.data[p].name);
          for (p = 0; t.data[0].labels.length > p; p++)
            l.set(1, p + 2, t.data[0].labels[p]);
          for (var c = 0; t.data.length > c; c++)
            for (p = 0; t.data[c].values.length > p; p++)
              l.set(c + 2, p + 2, t.data[c].values[p]);
          i.plugs.intAddAnyResourceToParse(
            "ppt\\embeddings\\Microsoft_Excel_Worksheet" +
              GLOBAL_CHART_COUNT +
              ".xlsx",
            "file",
            e.options.tempDir + "sample" + GLOBAL_CHART_COUNT + ".xlsx",
            h,
            !1,
            !0
          ),
            i.plugs.intAddAnyResourceToParse(
              "ppt\\charts\\chart" + GLOBAL_CHART_COUNT + ".xml",
              "buffer",
              t,
              u,
              !0
            ),
            i.plugs.intAddAnyResourceToParse(
              "ppt\\charts\\_rels\\chart" + GLOBAL_CHART_COUNT + ".xml.rels",
              "buffer",
              [
                {
                  type:
                    "http://schemas.openxmlformats.org/officeDocument/2006/relationships/package",
                  target:
                    "../embeddings/Microsoft_Excel_Worksheet" +
                    GLOBAL_CHART_COUNT +
                    ".xlsx",
                },
              ],
              i.plugs.type.msoffice.cbMakeRels,
              !0
            ),
            i.type.msoffice.files_list.push(
              {
                name: "/ppt/charts/chart" + GLOBAL_CHART_COUNT + ".xml",
                type:
                  "application/vnd.openxmlformats-officedocument.drawingml.chart+xml",
                clear: "type",
              },
              {
                name:
                  "/ppt/embeddings/Microsoft_Excel_Worksheet" +
                  GLOBAL_CHART_COUNT +
                  ".xlsx",
                type:
                  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                ext: "xlsx",
                clear: "type",
              }
            ),
            i.pages[a].rels.push({
              type:
                "http://schemas.openxmlformats.org/officeDocument/2006/relationships/chart",
              target: "../charts/chart" + GLOBAL_CHART_COUNT + ".xml",
              clear: "data",
            }),
            o.save(function (e) {
              e ? o.cancel() : r();
            });
        }),
        (r.addText = function (e, t, r, o, l, p) {
          var c,
            d = i.pages[a].data.length;
          if (
            ((i.pages[a].data[d] = {}),
            (i.pages[a].data[d].type = "text"),
            (i.pages[a].data[d].text = e || ""),
            (i.pages[a].data[d].options = "object" === _typeof(t) ? t : {}),
            "string" == typeof t
              ? (i.pages[a].data[d].options.color = t)
              : "object" !== _typeof(t) &&
                void 0 !== r &&
                ((i.pages[a].data[d].options.x = t),
                (i.pages[a].data[d].options.y = r),
                void 0 !== o &&
                  void 0 !== l &&
                  ((i.pages[a].data[d].options.cx = o),
                  (i.pages[a].data[d].options.cy = l))),
            "object" === _typeof(p))
          )
            for (c in p) i.pages[a].data[d].options[c] = p[c];
          else if ("object" === _typeof(o) && void 0 === l)
            for (c in o) i.pages[a].data[d].options[c] = o[c];
          return (
            s(i.pages[a].data[d]), n(i.pages[a].data[d]), i.pages[a].data[d].api
          );
        }),
        (r.addShape = function (e, t, r, o, l, p) {
          var c,
            d = i.pages[a].data.length;
          if (
            ((i.pages[a].data[d] = {}),
            (i.pages[a].data[d].type = "text"),
            (i.pages[a].data[d].options = "object" === _typeof(t) ? t : {}),
            (i.pages[a].data[d].options.shape = e),
            "string" == typeof t
              ? (i.pages[a].data[d].options.color = t)
              : "object" !== _typeof(t) &&
                void 0 !== r &&
                ((i.pages[a].data[d].options.x = t),
                (i.pages[a].data[d].options.y = r),
                void 0 !== o &&
                  void 0 !== l &&
                  ((i.pages[a].data[d].options.cx = o),
                  (i.pages[a].data[d].options.cy = l))),
            "object" === _typeof(p))
          )
            for (c in p) i.pages[a].data[d].options[c] = p[c];
          else if ("object" === _typeof(o) && void 0 === l)
            for (c in o) i.pages[a].data[d].options[c] = o[c];
          return (
            s(i.pages[a].data[d]), n(i.pages[a].data[d]), i.pages[a].data[d].api
          );
        }),
        (r.addImage = function (e, t, r, o, l, p) {
          var c,
            d = i.pages[a].data.length,
            m = "string" == typeof p ? p : "png",
            f = 0;
          if ("string" == typeof e) {
            var w = fast_image_size(e);
            if ("unknown" === w.type)
              switch (path.extname(e)) {
                case ".bmp":
                  m = "bmp";
                  break;
                case ".gif":
                  m = "gif";
                  break;
                case ".jpg":
                case ".jpeg":
                  m = "jpeg";
                  break;
                case ".emf":
                  m = "emf";
                  break;
                case ".tiff":
                  m = "tiff";
              }
            else
              w.width && (c = w.width),
                w.height && (f = w.height),
                "jpg" === (m = w.type) && (m = "jpeg");
          }
          (i.pages[a].data[d] = {}),
            (i.pages[a].data[d].type = "image"),
            (i.pages[a].data[d].image = e),
            (i.pages[a].data[d].options = "object" === _typeof(t) ? t : {}),
            !i.pages[a].data[d].options.cx &&
              c &&
              (i.pages[a].data[d].options.cx = c),
            !i.pages[a].data[d].options.cy &&
              f &&
              (i.pages[a].data[d].options.cy = f);
          var u = i.type.msoffice.src_files_list.indexOf(e),
            h = -1;
          if (u < 0)
            (i.type.msoffice.src_files_list[
              (u = i.type.msoffice.src_files_list.length)
            ] = e),
              i.plugs.intAddAnyResourceToParse(
                "ppt\\media\\image" + (u + 1) + "." + m,
                "string" == typeof e ? "file" : "stream",
                e,
                null,
                !1
              );
          else
            for (var g = 0, y = i.pages[a].rels.length; y > g; g++)
              i.pages[a].rels[g].target ===
                "../media/image" + (u + 1) + "." + m && (h = g + 1);
          if (
            (-1 === h &&
              ((h = i.pages[a].rels.length + 1),
              i.pages[a].rels.push({
                type:
                  "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image",
                target: "../media/image" + (u + 1) + "." + m,
                clear: "data",
              })),
            (t || {}).link)
          ) {
            var x = i.pages[a].rels.length + 1;
            i.pages[a].rels.push({
              type:
                "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink",
              target: t.link,
              targetMode: "External",
            }),
              (i.pages[a].data[d].link_rel_id = x);
          }
          return (
            (i.pages[a].data[d].image_id = u),
            (i.pages[a].data[d].rel_id = h),
            "string" == typeof t
              ? (i.pages[a].data[d].options.color = t)
              : "object" !== _typeof(t) &&
                void 0 !== r &&
                ((i.pages[a].data[d].options.x = t),
                (i.pages[a].data[d].options.y = r),
                void 0 !== o &&
                  void 0 !== l &&
                  ((i.pages[a].data[d].options.cx = o),
                  (i.pages[a].data[d].options.cy = l))),
            s(i.pages[a].data[d]),
            n(i.pages[a].data[d]),
            i.pages[a].data[d].api
          );
        }),
        (r.addP = function (e, t, r, s, n, o) {
          var l,
            p = i.pages[a].data.length;
          if (
            ((i.pages[a].data[p] = {}),
            (i.pages[a].data[p].type = "p"),
            (i.pages[a].data[p].data = []),
            (i.pages[a].data[p].options = "object" === _typeof(t) ? t : {}),
            "string" == typeof t
              ? (i.pages[a].data[p].options.color = t)
              : "object" !== _typeof(t) &&
                void 0 !== r &&
                ((i.pages[a].data[p].options.x = t),
                (i.pages[a].data[p].options.y = r),
                void 0 !== s &&
                  void 0 !== n &&
                  ((i.pages[a].data[p].options.cx = s),
                  (i.pages[a].data[p].options.cy = n))),
            "object" === _typeof(o))
          )
            for (l in o) i.pages[a].data[p].options[l] = o[l];
          else if ("object" === _typeof(s) && void 0 === n)
            for (l in s) i.pages[a].data[p].options[l] = s[l];
          return i.pages[a].data[p].data;
        }),
        (r.addDateToHeader = function () {
          i.pages[a].header || (i.pages[a].header = {});
        }),
        i.plugs.intAddAnyResourceToParse(
          "ppt\\slides\\slide" + (a + 1) + ".xml",
          "buffer",
          i.pages[a],
          w,
          !1
        ),
        i.plugs.intAddAnyResourceToParse(
          "ppt\\slides\\_rels\\slide" + (a + 1) + ".xml.rels",
          "buffer",
          i.pages[a].rels,
          i.plugs.type.msoffice.cbMakeRels,
          !1
        ),
        i.features.type.pptx.emitEvent("newPage", {
          genobj: e,
          page: r,
          pageData: i.pages[a],
          pageNumber: a,
          slideOptions: t,
        }),
        r
      );
    }),
    i.features.type.pptx.emitEvent("makeDocApi", e),
    this
  );
}
(void 0)("pptx", makePptx, {}, void 0, "Microsoft PowerPoint Document"),
  (void 0)(
    "ppsx",
    makePptx,
    {},
    void 0,
    "Microsoft PowerPoint Slideshow Document"
  );
var genpptx = Object.freeze({}),
  baseobj$1 = require("./basicgen.js"),
  msdoc$1 = require("./msofficegen.js"),
  docplugman$1 = require("./docplug");
function encodeHTML$1(e) {
  return e
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;");
}
function makeXlsx(e, t, a, i, r) {
  function s(t) {
    for (
      var a =
          i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(t) +
          '<sst xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" count="' +
          e.generate_data.total_strings +
          '" uniqueCount="' +
          e.generate_data.shared_strings.length +
          '">',
        r = 0,
        s = e.generate_data.shared_strings.length;
      s > r;
      r++
    )
      a +=
        "<si><t>" +
        encodeHTML$1(e.generate_data.shared_strings[r]) +
        "</t></si>";
    return a + "</sst>";
  }
  function n(e, t) {
    for (var a = 0, i = 0, r = e.length, s = 0; r > i; ) {
      var n = e.charCodeAt(i);
      if (n >= 48 && 57 >= n) {
        s = (s = parseInt(e.slice(i), 10)) > 0 ? s - 1 : 0;
        break;
      }
      65 > n || n > 90
        ? 97 > n || n > 122 || (i > 0 && (a++, (a *= 26)), (a += n - 97))
        : (i > 0 && (a++, (a *= 26)), (a += n - 65)),
        i++;
    }
    return t ? { row: s, column: a } : a;
  }
  function o(e) {
    for (
      var t = "", a = e;
      a >= 0 && ((t = String.fromCharCode((a % 26) + 65) + t), a >= 26);

    )
      a = Math.floor(a / 26) - 1;
    return t;
  }
  function l(t) {
    var a,
      r,
      s,
      n,
      l,
      p,
      c =
        i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(t) +
        '<worksheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships">',
      d = 0,
      m = 0;
    for (
      a = t.sheet.data.length ? t.sheet.data.length - 1 : 0,
        s = 0,
        l = t.sheet.data.length;
      l > s;
      s++
    )
      t.sheet.data[s] &&
        (d =
          (r = t.sheet.data[s].length ? t.sheet.data[s].length - 1 : 0) > d
            ? r
            : d);
    for (
      c += '<dimension ref="A1:' + o(d) + (a + 1) + '"/><sheetViews>',
        c += '<sheetView tabSelected="1" workbookViewId="0"/>',
        c += '</sheetViews><sheetFormatPr defaultRowHeight="15"/>',
        t.sheet.width &&
          (t.sheet.width.forEach(function (e, t) {
            if ("object" === _typeof(e)) {
              var a = "";
              "number" == typeof e.width &&
                e.width == e.width &&
                (a = ' width="' + e.width + '" customWidth="1"'),
                "number" == typeof e.styleCode &&
                  e.styleCode == e.styleCode &&
                  (a = ' style="' + e.styleCode + '"'),
                m || (c += "<cols>"),
                (c +=
                  '<col min="' +
                  (e.colId + 1) +
                  '" max="' +
                  (e.colId + 1) +
                  '"' +
                  a +
                  "/>"),
                m++;
            } else "number" == typeof e && (m || (c += "<cols>"), (c += '<col min="' + (t + 1) + '" max="' + (t + 1) + '" width="' + e + '" customWidth="1"/>'), m++);
          }),
          m && (c += "</cols>")),
        c += "<sheetData>",
        s = 0,
        l = t.sheet.data.length;
      l > s;
      s++
    )
      if (t.sheet.data[s]) {
        var f = 1;
        for (
          t.sheet.data[s].forEach(function (e) {
            if ("string" == typeof e) {
              var t = e.split("\n").length;
              f = Math.max(f, t);
            }
          }),
            c +=
              '<row r="' +
              (s + 1) +
              '" spans="1:' +
              t.sheet.data[s].length +
              '" ht="' +
              15 * f +
              '">',
            n = 0,
            p = t.sheet.data[s].length;
          p > n;
          n++
        ) {
          var w = t.sheet.data[s][n];
          if (void 0 !== w) {
            var u = "",
              h = "0";
            switch (_typeof(w)) {
              case "number":
                h = w;
                break;
              case "string":
                (h = e.generate_data.cell_strings[t.id][s][n]),
                  (u = 0 > w.indexOf("\n") ? ' t="s"' : ' s="1" t="s"');
            }
            c += '<c r="' + o(n) + (s + 1) + '"' + u + "><v>" + h + "</v></c>";
          }
        }
        c += "</row>";
      }
    return (c +=
      '</sheetData><pageMargins left="0.7" right="0.7" top="0.75" bottom="0.75" header="0.3" footer="0.3"/></worksheet>');
  }
  msdoc$1.makemsdoc(e, t, a, i, r),
    i.plugs.type.msoffice.makeOfficeGenerator("xl", "workbook", {}),
    (i.features.page_name = "sheets"),
    e.on("beforeGen", function () {
      (e.generate_data = {}),
        (e.generate_data.shared_strings = []),
        (e.lookup_strings = {}),
        (e.generate_data.total_strings = 0),
        (e.generate_data.cell_strings = []),
        i.features.type.xlsx.emitEvent("beforeGen", e),
        i.features.type.xlsx.emitEvent("beforeGenFinal", e);
      for (var t = 0, a = i.pages.length; a > t; t++)
        if (i.pages[t])
          for (var r = 0, n = i.pages[t].sheet.data.length; n > r; r++)
            if (i.pages[t].sheet.data[r])
              for (var o = 0, l = i.pages[t].sheet.data[r].length; l > o; o++)
                if (void 0 !== i.pages[t].sheet.data[r][o])
                  switch (_typeof(i.pages[t].sheet.data[r][o])) {
                    case "string":
                      e.generate_data.total_strings++,
                        e.generate_data.cell_strings[t] ||
                          (e.generate_data.cell_strings[t] = []),
                        e.generate_data.cell_strings[t][r] ||
                          (e.generate_data.cell_strings[t][r] = []);
                      var p = i.pages[t].sheet.data[r][o];
                      if (p in e.lookup_strings)
                        e.generate_data.cell_strings[t][r][o] =
                          e.lookup_strings[p];
                      else {
                        var c = e.generate_data.shared_strings.length;
                        (e.generate_data.cell_strings[t][r][o] = c),
                          (e.lookup_strings[p] = c),
                          (e.generate_data.shared_strings[c] = p);
                      }
                  }
      e.generate_data.total_strings &&
        (i.plugs.intAddAnyResourceToParse(
          "xl\\sharedStrings.xml",
          "buffer",
          null,
          s,
          !1
        ),
        i.type.msoffice.files_list.push({
          name: "/xl/sharedStrings.xml",
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml",
          clear: "generate",
        }),
        i.type.msoffice.rels_app.push({
          type:
            "http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings",
          target: "sharedStrings.xml",
          clear: "generate",
        }));
    });
  var p = new docplugman$1(e, i, "xlsx", function (e) {});
  return (
    a.extraPlugs &&
      "object" === _typeof(a.extraPlugs) &&
      a.extraPlugs.forEach &&
      a.extraPlugs.forEach(function (e) {
        var t;
        e &&
          ("function" == typeof e
            ? (t = e)
            : "string" == typeof e && (t = require("./" + e))),
          p.plugsList.push(new t(p));
      }),
    i.type.msoffice.files_list.push(
      {
        name: "/xl/styles.xml",
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml",
        clear: "type",
      },
      {
        name: "/xl/workbook.xml",
        type:
          "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml",
        clear: "type",
      }
    ),
    i.type.msoffice.rels_app.push(
      {
        type:
          "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles",
        target: "styles.xml",
        clear: "type",
      },
      {
        type:
          "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme",
        target: "theme/theme1.xml",
        clear: "type",
      }
    ),
    i.plugs.intAddAnyResourceToParse(
      "docProps\\app.xml",
      "buffer",
      null,
      function (t) {
        for (
          var a = i.pages.length,
            r = e.options.author || e.options.creator || "officegen",
            s =
              i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(t) +
              '<Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes"><Application>Microsoft Excel</Application><DocSecurity>0</DocSecurity><ScaleCrop>false</ScaleCrop><HeadingPairs><vt:vector size="2" baseType="variant"><vt:variant><vt:lpstr>Worksheets</vt:lpstr></vt:variant><vt:variant><vt:i4>' +
              a +
              '</vt:i4></vt:variant></vt:vector></HeadingPairs><TitlesOfParts><vt:vector size="' +
              a +
              '" baseType="lpstr">',
            n = 0,
            o = i.pages.length;
          o > n;
          n++
        )
          s += "<vt:lpstr>Sheet" + (n + 1) + "</vt:lpstr>";
        return (s +=
          "</vt:vector></TitlesOfParts><Company>" +
          r +
          "</Company><LinksUpToDate>false</LinksUpToDate><SharedDoc>false</SharedDoc><HyperlinksChanged>false</HyperlinksChanged><AppVersion>12.0000</AppVersion></Properties>");
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "xl\\styles.xml",
      "buffer",
      null,
      function (e) {
        return (
          i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
          '<styleSheet xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main"><fonts count="1"><font><sz val="11"/><color theme="1"/><name val="Calibri"/><family val="2"/><scheme val="minor"/></font></fonts><fills count="2"><fill><patternFill patternType="none"/></fill><fill><patternFill patternType="gray125"/></fill></fills><borders count="1"><border><left/><right/><top/><bottom/><diagonal/></border></borders><cellStyleXfs count="1"><xf numFmtId="0" fontId="0" fillId="0" borderId="0"/></cellStyleXfs><cellXfs count="2"><xf numFmtId="0" fontId="0" fillId="0" borderId="0" xfId="0"/><xf applyAlignment="1" borderId="0" fillId="0" fontId="0" numFmtId="0" xfId="0"><alignment wrapText="1"/></xf></cellXfs><cellStyles count="1"><cellStyle name="Normal" xfId="0" builtinId="0"/></cellStyles><dxfs count="0"/><tableStyles count="0" defaultTableStyle="TableStyleMedium9" defaultPivotStyle="PivotStyleLight16"/></styleSheet>'
        );
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "xl\\workbook.xml",
      "buffer",
      null,
      function (e) {
        for (
          var t =
              i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
              '<workbook xmlns="http://schemas.openxmlformats.org/spreadsheetml/2006/main" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"><fileVersion appName="xl" lastEdited="4" lowestEdited="4" rupBuild="4507"/><workbookPr defaultThemeVersion="124226"/><bookViews><workbookView xWindow="120" yWindow="75" windowWidth="19095" windowHeight="7485"/></bookViews><sheets>',
            a = 0,
            r = i.pages.length;
          r > a;
          a++
        )
          t +=
            '<sheet name="' +
            (i.pages[a].sheet.name || "Sheet" + (a + 1)) +
            '" sheetId="' +
            (a + 1) +
            '" r:id="rId' +
            i.pages[a].relId +
            '"/>';
        return (t += '</sheets><calcPr calcId="125725"/></workbook>');
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "xl\\_rels\\workbook.xml.rels",
      "buffer",
      i.type.msoffice.rels_app,
      i.plugs.type.msoffice.cbMakeRels,
      !0
    ),
    (e.makeNewSheet = function () {
      var t = i.pages.length,
        a = { data: [], width: [] };
      return (
        (i.pages[t] = {}),
        (i.pages[t].id = t),
        (i.pages[t].relId = i.type.msoffice.rels_app.length + 1),
        (i.pages[t].sheet = a),
        i.type.msoffice.rels_app.push({
          type:
            "http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet",
          target: "worksheets/sheet" + (t + 1) + ".xml",
          clear: "data",
        }),
        i.type.msoffice.files_list.push({
          name: "/xl/worksheets/sheet" + (t + 1) + ".xml",
          type:
            "application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml",
          clear: "data",
        }),
        (a.setColumnWidth = function (e, t) {
          var i = null;
          (e = n(e + "1", !1)),
            a.width.every(function (t) {
              return t.colId !== e || ((i = t), !1);
            }),
            i ? (i.width = t) : a.width.push({ colId: e, width: t });
        }),
        (a.setColumnCenter = function (e) {
          var t = null;
          (e = n(e + "1", !1)),
            a.width.every(function (a) {
              return a.colId !== e || ((t = a), !1);
            }),
            t
              ? (t.styleCode = 1)
              : a.width.push({ colId: e, width: 9.140625, styleCode: 1 });
        }),
        (a.setCell = function (e, t) {
          var i = n(e, !0);
          a.data[i.row] || (a.data[i.row] = []), (a.data[i.row][i.column] = t);
        }),
        i.plugs.intAddAnyResourceToParse(
          "xl\\worksheets\\sheet" + (t + 1) + ".xml",
          "buffer",
          i.pages[t],
          l,
          !1
        ),
        i.features.type.xlsx.emitEvent("newPage", {
          genobj: e,
          page: a,
          pageData: i.pages[t],
          pageNumber: t,
        }),
        a
      );
    }),
    i.features.type.xlsx.emitEvent("makeDocApi", e),
    this
  );
}
baseobj$1.plugins.registerDocType(
  "xlsx",
  makeXlsx,
  {},
  baseobj$1.docType.SPREADSHEET,
  "Microsoft Excel Document"
);
var genxlsx = Object.freeze({}),
  baseobj$2 = require("./basicgen.js"),
  msdoc$2 = require("./msofficegen.js"),
  docxP = require("./docx-p.js"),
  docxTable = require("./docxtable.js"),
  xmlBuilder$1 = require("xmlbuilder"),
  docplugman$2 = require("./docplug"),
  plugHeadfoot = require("./docxplg-headfoot");
function encodeHTML$2(e) {
  return e
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;");
}
var defaultStyleXML =
  '<w:styles xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"><w:docDefaults><w:rPrDefault><w:rPr><w:rFonts w:asciiTheme="minorHAnsi" w:eastAsiaTheme="minorHAnsi" w:hAnsiTheme="minorHAnsi" w:cstheme="minorBidi"/><w:sz w:val="22"/><w:szCs w:val="22"/><w:lang w:val="en-US" w:eastAsia="en-US" w:bidi="en-US"/></w:rPr></w:rPrDefault><w:pPrDefault><w:pPr><w:spacing w:after="200" w:line="276" w:lineRule="auto"/></w:pPr></w:pPrDefault></w:docDefaults><w:latentStyles w:defLockedState="0" w:defUIPriority="99" w:defSemiHidden="1" w:defUnhideWhenUsed="1" w:defQFormat="0" w:count="267"><w:lsdException w:name="Normal" w:semiHidden="0" w:uiPriority="0" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="heading 1" w:semiHidden="0" w:uiPriority="9" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="heading 2" w:uiPriority="9" w:qFormat="1"/><w:lsdException w:name="heading 3" w:uiPriority="9" w:qFormat="1"/><w:lsdException w:name="heading 4" w:uiPriority="9" w:qFormat="1"/><w:lsdException w:name="heading 5" w:uiPriority="9" w:qFormat="1"/><w:lsdException w:name="heading 6" w:uiPriority="9" w:qFormat="1"/><w:lsdException w:name="heading 7" w:uiPriority="9" w:qFormat="1"/><w:lsdException w:name="heading 8" w:uiPriority="9" w:qFormat="1"/><w:lsdException w:name="heading 9" w:uiPriority="9" w:qFormat="1"/><w:lsdException w:name="toc 1" w:uiPriority="39"/><w:lsdException w:name="toc 2" w:uiPriority="39"/><w:lsdException w:name="toc 3" w:uiPriority="39"/><w:lsdException w:name="toc 4" w:uiPriority="39"/><w:lsdException w:name="toc 5" w:uiPriority="39"/><w:lsdException w:name="toc 6" w:uiPriority="39"/><w:lsdException w:name="toc 7" w:uiPriority="39"/><w:lsdException w:name="toc 8" w:uiPriority="39"/><w:lsdException w:name="toc 9" w:uiPriority="39"/><w:lsdException w:name="caption" w:uiPriority="35" w:qFormat="1"/><w:lsdException w:name="Title" w:semiHidden="0" w:uiPriority="10" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Default Paragraph Font" w:uiPriority="1"/><w:lsdException w:name="Subtitle" w:semiHidden="0" w:uiPriority="11" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Strong" w:semiHidden="0" w:uiPriority="22" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Emphasis" w:semiHidden="0" w:uiPriority="20" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Table Grid" w:semiHidden="0" w:uiPriority="59" w:unhideWhenUsed="0"/><w:lsdException w:name="Placeholder Text" w:unhideWhenUsed="0"/><w:lsdException w:name="No Spacing" w:semiHidden="0" w:uiPriority="1" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Light Shading" w:semiHidden="0" w:uiPriority="60" w:unhideWhenUsed="0"/><w:lsdException w:name="Light List" w:semiHidden="0" w:uiPriority="61" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Grid" w:semiHidden="0" w:uiPriority="62" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 1" w:semiHidden="0" w:uiPriority="63" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 2" w:semiHidden="0" w:uiPriority="64" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 1" w:semiHidden="0" w:uiPriority="65" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 2" w:semiHidden="0" w:uiPriority="66" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 1" w:semiHidden="0" w:uiPriority="67" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 2" w:semiHidden="0" w:uiPriority="68" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 3" w:semiHidden="0" w:uiPriority="69" w:unhideWhenUsed="0"/><w:lsdException w:name="Dark List" w:semiHidden="0" w:uiPriority="70" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Shading" w:semiHidden="0" w:uiPriority="71" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful List" w:semiHidden="0" w:uiPriority="72" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Grid" w:semiHidden="0" w:uiPriority="73" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Shading Accent 1" w:semiHidden="0" w:uiPriority="60" w:unhideWhenUsed="0"/><w:lsdException w:name="Light List Accent 1" w:semiHidden="0" w:uiPriority="61" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Grid Accent 1" w:semiHidden="0" w:uiPriority="62" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 1 Accent 1" w:semiHidden="0" w:uiPriority="63" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 2 Accent 1" w:semiHidden="0" w:uiPriority="64" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 1 Accent 1" w:semiHidden="0" w:uiPriority="65" w:unhideWhenUsed="0"/><w:lsdException w:name="Revision" w:unhideWhenUsed="0"/><w:lsdException w:name="List Paragraph" w:semiHidden="0" w:uiPriority="34" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Quote" w:semiHidden="0" w:uiPriority="29" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Intense Quote" w:semiHidden="0" w:uiPriority="30" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Medium List 2 Accent 1" w:semiHidden="0" w:uiPriority="66" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 1 Accent 1" w:semiHidden="0" w:uiPriority="67" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 2 Accent 1" w:semiHidden="0" w:uiPriority="68" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 3 Accent 1" w:semiHidden="0" w:uiPriority="69" w:unhideWhenUsed="0"/><w:lsdException w:name="Dark List Accent 1" w:semiHidden="0" w:uiPriority="70" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Shading Accent 1" w:semiHidden="0" w:uiPriority="71" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful List Accent 1" w:semiHidden="0" w:uiPriority="72" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Grid Accent 1" w:semiHidden="0" w:uiPriority="73" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Shading Accent 2" w:semiHidden="0" w:uiPriority="60" w:unhideWhenUsed="0"/><w:lsdException w:name="Light List Accent 2" w:semiHidden="0" w:uiPriority="61" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Grid Accent 2" w:semiHidden="0" w:uiPriority="62" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 1 Accent 2" w:semiHidden="0" w:uiPriority="63" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 2 Accent 2" w:semiHidden="0" w:uiPriority="64" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 1 Accent 2" w:semiHidden="0" w:uiPriority="65" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 2 Accent 2" w:semiHidden="0" w:uiPriority="66" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 1 Accent 2" w:semiHidden="0" w:uiPriority="67" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 2 Accent 2" w:semiHidden="0" w:uiPriority="68" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 3 Accent 2" w:semiHidden="0" w:uiPriority="69" w:unhideWhenUsed="0"/><w:lsdException w:name="Dark List Accent 2" w:semiHidden="0" w:uiPriority="70" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Shading Accent 2" w:semiHidden="0" w:uiPriority="71" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful List Accent 2" w:semiHidden="0" w:uiPriority="72" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Grid Accent 2" w:semiHidden="0" w:uiPriority="73" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Shading Accent 3" w:semiHidden="0" w:uiPriority="60" w:unhideWhenUsed="0"/><w:lsdException w:name="Light List Accent 3" w:semiHidden="0" w:uiPriority="61" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Grid Accent 3" w:semiHidden="0" w:uiPriority="62" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 1 Accent 3" w:semiHidden="0" w:uiPriority="63" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 2 Accent 3" w:semiHidden="0" w:uiPriority="64" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 1 Accent 3" w:semiHidden="0" w:uiPriority="65" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 2 Accent 3" w:semiHidden="0" w:uiPriority="66" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 1 Accent 3" w:semiHidden="0" w:uiPriority="67" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 2 Accent 3" w:semiHidden="0" w:uiPriority="68" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 3 Accent 3" w:semiHidden="0" w:uiPriority="69" w:unhideWhenUsed="0"/><w:lsdException w:name="Dark List Accent 3" w:semiHidden="0" w:uiPriority="70" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Shading Accent 3" w:semiHidden="0" w:uiPriority="71" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful List Accent 3" w:semiHidden="0" w:uiPriority="72" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Grid Accent 3" w:semiHidden="0" w:uiPriority="73" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Shading Accent 4" w:semiHidden="0" w:uiPriority="60" w:unhideWhenUsed="0"/><w:lsdException w:name="Light List Accent 4" w:semiHidden="0" w:uiPriority="61" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Grid Accent 4" w:semiHidden="0" w:uiPriority="62" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 1 Accent 4" w:semiHidden="0" w:uiPriority="63" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 2 Accent 4" w:semiHidden="0" w:uiPriority="64" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 1 Accent 4" w:semiHidden="0" w:uiPriority="65" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 2 Accent 4" w:semiHidden="0" w:uiPriority="66" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 1 Accent 4" w:semiHidden="0" w:uiPriority="67" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 2 Accent 4" w:semiHidden="0" w:uiPriority="68" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 3 Accent 4" w:semiHidden="0" w:uiPriority="69" w:unhideWhenUsed="0"/><w:lsdException w:name="Dark List Accent 4" w:semiHidden="0" w:uiPriority="70" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Shading Accent 4" w:semiHidden="0" w:uiPriority="71" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful List Accent 4" w:semiHidden="0" w:uiPriority="72" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Grid Accent 4" w:semiHidden="0" w:uiPriority="73" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Shading Accent 5" w:semiHidden="0" w:uiPriority="60" w:unhideWhenUsed="0"/><w:lsdException w:name="Light List Accent 5" w:semiHidden="0" w:uiPriority="61" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Grid Accent 5" w:semiHidden="0" w:uiPriority="62" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 1 Accent 5" w:semiHidden="0" w:uiPriority="63" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 2 Accent 5" w:semiHidden="0" w:uiPriority="64" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 1 Accent 5" w:semiHidden="0" w:uiPriority="65" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 2 Accent 5" w:semiHidden="0" w:uiPriority="66" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 1 Accent 5" w:semiHidden="0" w:uiPriority="67" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 2 Accent 5" w:semiHidden="0" w:uiPriority="68" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 3 Accent 5" w:semiHidden="0" w:uiPriority="69" w:unhideWhenUsed="0"/><w:lsdException w:name="Dark List Accent 5" w:semiHidden="0" w:uiPriority="70" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Shading Accent 5" w:semiHidden="0" w:uiPriority="71" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful List Accent 5" w:semiHidden="0" w:uiPriority="72" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Grid Accent 5" w:semiHidden="0" w:uiPriority="73" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Shading Accent 6" w:semiHidden="0" w:uiPriority="60" w:unhideWhenUsed="0"/><w:lsdException w:name="Light List Accent 6" w:semiHidden="0" w:uiPriority="61" w:unhideWhenUsed="0"/><w:lsdException w:name="Light Grid Accent 6" w:semiHidden="0" w:uiPriority="62" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 1 Accent 6" w:semiHidden="0" w:uiPriority="63" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Shading 2 Accent 6" w:semiHidden="0" w:uiPriority="64" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 1 Accent 6" w:semiHidden="0" w:uiPriority="65" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium List 2 Accent 6" w:semiHidden="0" w:uiPriority="66" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 1 Accent 6" w:semiHidden="0" w:uiPriority="67" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 2 Accent 6" w:semiHidden="0" w:uiPriority="68" w:unhideWhenUsed="0"/><w:lsdException w:name="Medium Grid 3 Accent 6" w:semiHidden="0" w:uiPriority="69" w:unhideWhenUsed="0"/><w:lsdException w:name="Dark List Accent 6" w:semiHidden="0" w:uiPriority="70" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Shading Accent 6" w:semiHidden="0" w:uiPriority="71" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful List Accent 6" w:semiHidden="0" w:uiPriority="72" w:unhideWhenUsed="0"/><w:lsdException w:name="Colorful Grid Accent 6" w:semiHidden="0" w:uiPriority="73" w:unhideWhenUsed="0"/><w:lsdException w:name="Subtle Emphasis" w:semiHidden="0" w:uiPriority="19" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Intense Emphasis" w:semiHidden="0" w:uiPriority="21" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Subtle Reference" w:semiHidden="0" w:uiPriority="31" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Intense Reference" w:semiHidden="0" w:uiPriority="32" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Book Title" w:semiHidden="0" w:uiPriority="33" w:unhideWhenUsed="0" w:qFormat="1"/><w:lsdException w:name="Bibliography" w:uiPriority="37"/><w:lsdException w:name="TOC Heading" w:uiPriority="39" w:qFormat="1"/></w:latentStyles><w:style w:type="paragraph" w:default="1" w:styleId="Normal"><w:name w:val="Normal"/><w:qFormat/><w:rsid w:val="00A02F19"/></w:style><w:style w:type="character" w:default="1" w:styleId="DefaultParagraphFont"><w:name w:val="Default Paragraph Font"/><w:uiPriority w:val="1"/><w:semiHidden/><w:unhideWhenUsed/></w:style><w:style w:type="table" w:default="1" w:styleId="TableNormal"><w:name w:val="Normal Table"/><w:uiPriority w:val="99"/><w:semiHidden/><w:unhideWhenUsed/><w:qFormat/><w:tblPr><w:tblInd w:w="0" w:type="dxa"/><w:tblCellMar><w:top w:w="0" w:type="dxa"/><w:left w:w="108" w:type="dxa"/><w:bottom w:w="0" w:type="dxa"/><w:right w:w="108" w:type="dxa"/></w:tblCellMar></w:tblPr></w:style><w:style w:type="numbering" w:default="1" w:styleId="NoList"><w:name w:val="No List"/><w:uiPriority w:val="99"/><w:semiHidden/><w:unhideWhenUsed/></w:style></w:styles>';
function makeDocx(e, t, a, i, r) {
  function s(e) {
    (e.docStartExtra = e.docStartExtra || ""),
      (e.docEndExtra = e.docEndExtra || "");
    var t,
      r =
        i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
        "<w:" +
        e.docType +
        ' xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">' +
        e.docStartExtra,
      s = e.data,
      n = 0;
    s.length ||
      ((r += '<w:p w:rsidR="009F2180" w:rsidRDefault="009F2180">'),
      e.pStyleDef &&
        (r += '<w:pPr><w:pStyle w:val="' + e.pStyleDef + '"/></w:pPr>'),
      (r += "</w:p>"));
    for (var l = 0, p = s.length; p > l; l++)
      if (s[l] && "table" === s[l].type) {
        var c = docxTable.getTable(s[l].data, s[l].options);
        r += xmlBuilder$1
          .create(c, { version: "1.0", encoding: "UTF-8", standalone: !0 })
          .toString({ pretty: !0, indent: "  ", newline: "\n" });
      } else {
        r += '<w:p w:rsidR="00A77427" w:rsidRDefault="007F1D13">';
        var d = "";
        if (s[l].options) {
          if (
            (s[l].options.indentLeft &&
              (d += '<w:ind w:left="' + s[l].options.indentLeft + '"/>'),
            s[l].options.align)
          )
            switch (s[l].options.align) {
              case "center":
                d += '<w:jc w:val="center"/>';
                break;
              case "right":
                d += '<w:jc w:val="right"/>';
                break;
              case "justify":
                d += '<w:jc w:val="both"/>';
            }
          s[l].options.list_type &&
            (d +=
              '<w:pStyle w:val="ListParagraph"/><w:numPr><w:ilvl w:val="0"/><w:numId w:val="' +
              s[l].options.list_type +
              '"/></w:numPr>'),
            s[l].options.backline &&
              (d +=
                '<w:pPr><w:shd w:val="solid" w:color="' +
                s[l].options.backline +
                '" w:fill="auto"/></w:pPr>'),
            s[l].options.rtl && (d += '<w:bidi w:val="1"/>');
        }
        var m = (s[l].options && s[l].options.pStyleDef) || e.pStyleDef;
        !d && m && (d = '<w:pStyle w:val="' + m + '"/>'),
          d && (r += "<w:pPr>" + d + "</w:pPr>");
        for (var f = 0, w = s[l].data.length; w > f; f++)
          if (s[l].data[f]) {
            var u,
              h,
              g,
              y = "",
              x = "",
              P = !1;
            if (s[l].data[f].options) {
              if (
                (s[l].data[f].options.color &&
                  (x +=
                    '<w:color w:val="' + s[l].data[f].options.color + '"/>'),
                s[l].data[f].options.back &&
                  (x +=
                    '<w:shd w:val="' +
                    (h = s[l].data[f].options.shdType || "clear") +
                    '" w:color="' +
                    (u = s[l].data[f].options.shdColor || "auto") +
                    '" w:fill="' +
                    s[l].data[f].options.back +
                    '"/>'),
                s[l].data[f].options.highlight &&
                  ((h = "yellow"),
                  "string" == typeof s[l].data[f].options.highlight &&
                    (h = s[l].data[f].options.highlight),
                  (x += '<w:highlight w:val="' + h + '"/>')),
                s[l].data[f].options.bold && (x += "<w:b/><w:bCs/>"),
                s[l].data[f].options.italic && (x += "<w:i/><w:iCs/>"),
                s[l].data[f].options.underline &&
                  ((h = "single"),
                  "string" == typeof s[l].data[f].options.underline &&
                    (h = s[l].data[f].options.underline),
                  (x += '<w:u w:val="' + h + '"/>')),
                s[l].data[f].options.strikethrough && (x += "<w:strike/>"),
                s[l].data[f].options.font_face &&
                  (x +=
                    '<w:rFonts w:ascii="' +
                    s[l].data[f].options.font_face +
                    '" w:eastAsia="' +
                    s[l].data[f].options.font_face +
                    '" w:hAnsi="' +
                    s[l].data[f].options.font_face +
                    '" w:cs="' +
                    s[l].data[f].options.font_face +
                    '"/>'),
                s[l].data[f].options.font_size)
              ) {
                var v = 2 * s[l].data[f].options.font_size;
                x += '<w:sz w:val="' + v + '"/><w:szCs w:val="' + v + '"/>';
              }
              s[l].data[f].options.border &&
                ((u = "auto"),
                (h = "single"),
                (g = 4),
                "string" == typeof s[l].data[f].options.borderColor &&
                  (u = s[l].data[f].options.borderColor),
                "string" == typeof s[l].data[f].options.border &&
                  (h = s[l].data[f].options.border),
                "number" == typeof s[l].data[f].options.borderSize &&
                  s[l].data[f].options.borderSize &&
                  s[l].data[f].options.borderSize ==
                    s[l].data[f].options.borderSize &&
                  (g = s[l].data[f].options.borderSize),
                (x +=
                  '<w:bdr w:val="' +
                  h +
                  '" w:sz="' +
                  g +
                  '" w:space="0" w:color="' +
                  u +
                  '"/>')),
                s[l].data[f].options.hyperlink &&
                  ((r +=
                    '<w:hyperlink w:anchor="' +
                    s[l].data[f].options.hyperlink +
                    '">'),
                  (P = !0),
                  x || (x = '<w:rStyle w:val="Hyperlink"/>')),
                s[l].data[f].options.rtl && (x += '<w:rtl w:val="1"/>');
            }
            if (
              (s[l].data[f].fieldObj &&
                (r += '<w:fldSimple w:instr="' + s[l].data[f].fieldObj + '">'),
              s[l].data[f].text)
            )
              (" " !== s[l].data[f].text[0] &&
                " " !== s[l].data[f].text[s[l].data[f].text.length - 1]) ||
                (y += ' xml:space="preserve"'),
                s[l].data[f].link_rel_id &&
                  (r +=
                    '<w:hyperlink r:id="rId' + s[l].data[f].link_rel_id + '">'),
                (r += "<w:r>"),
                x && (r += "<w:rPr>" + x + "</w:rPr>"),
                (r +=
                  "<w:t" +
                  y +
                  ">" +
                  encodeHTML$2(s[l].data[f].text) +
                  "</w:t></w:r>"),
                s[l].data[f].link_rel_id && (r += "</w:hyperlink>");
            else if (s[l].data[f].page_break)
              r += '<w:r><w:br w:type="page"/></w:r>';
            else if (s[l].data[f].line_break) r += "<w:r><w:br/></w:r>";
            else if (s[l].data[f].horizontal_line)
              r +=
                '<w:r><w:pict><v:rect style="width:0height:.75pt" o:hralign="center" o:hrstd="t" o:hr="t" fillcolor="#e0e0e0" stroked="f"/></w:pict></w:r>';
            else if (s[l].data[f].bookmark_start)
              r +=
                '<w:bookmarkStart w:id="' +
                n +
                '" w:name="' +
                s[l].data[f].bookmark_start +
                '"/>';
            else if (s[l].data[f].bookmark_end)
              (r += '<w:bookmarkEnd w:id="' + n + '"/>'), n++;
            else if (s[l].data[f].image) {
              (r += "<w:r>"),
                (x += "<w:noProof/>") && (r += "<w:rPr>" + x + "</w:rPr>");
              (r += "<w:drawing>"),
                (r += '<wp:inline distT="0" distB="0" distL="0" distR="0">'),
                (r +=
                  '<wp:extent cx="' +
                  Math.round(9525 * s[l].data[f].options.cx) +
                  '" cy="' +
                  Math.round(9525 * s[l].data[f].options.cy) +
                  '"/>'),
                (r += '<wp:effectExtent l="19050" t="0" r="9525" b="0"/>'),
                (r +=
                  '<wp:docPr id="' +
                  (s[l].data[f].image_id + 1) +
                  '" name="Picture ' +
                  s[l].data[f].image_id +
                  '" descr="Picture ' +
                  s[l].data[f].image_id +
                  '">'),
                s[l].data[f].link_rel_id &&
                  (r +=
                    '<a:hlinkClick xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" r:id="rId' +
                    s[l].data[f].link_rel_id +
                    '"/>'),
                (r += "</wp:docPr>"),
                (r += "<wp:cNvGraphicFramePr>"),
                (r +=
                  '<a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/>'),
                (r += "</wp:cNvGraphicFramePr>"),
                (r +=
                  '<a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">'),
                (r +=
                  '<a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">'),
                (r +=
                  '<pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">'),
                (r += "<pic:nvPicPr>"),
                (r +=
                  '<pic:cNvPr id="0" name="Picture ' +
                  s[l].data[f].image_id +
                  '"/>'),
                (r += "<pic:cNvPicPr/>"),
                (r += "</pic:nvPicPr>"),
                (r += "<pic:blipFill>"),
                (r +=
                  '<a:blip r:embed="rId' +
                  s[l].data[f].rel_id +
                  '" cstate="print"/>'),
                (r += "<a:stretch>"),
                (r += "<a:fillRect/>"),
                (r += "</a:stretch>"),
                (r += "</pic:blipFill>"),
                (r += "<pic:spPr>"),
                (r += "<a:xfrm>"),
                (r += '<a:off x="0" y="0"/>'),
                (r +=
                  '<a:ext cx="' +
                  9525 * s[l].data[f].options.cx +
                  '" cy="' +
                  9525 * s[l].data[f].options.cy +
                  '"/>'),
                (r += "</a:xfrm>"),
                (r += '<a:prstGeom prst="rect">'),
                (r += "<a:avLst/>"),
                (r += "</a:prstGeom>"),
                (r += "</pic:spPr>"),
                (r += "</pic:pic>"),
                (r += "</a:graphicData>"),
                (r += "</a:graphic>"),
                (r += "</wp:inline>"),
                (r += "</w:drawing>"),
                (r += "</w:r>");
            }
            s[l].data[f].fieldObj && (r += "</w:fldSimple>"),
              P && (r += "</w:hyperlink>");
          }
        r += "</w:p>";
      }
    "document" === e.docType &&
      ((r += '<w:p w:rsidR="00A02F19" w:rsidRDefault="00A02F19"/>'),
      (r +=
        a.orientation && "landscape" === a.orientation
          ? '<w:sectPr w:rsidR="00A02F19" w:rsidSect="00897086">' +
            (o.secPrExtra ? o.secPrExtra : "") +
            '<w:pgSz w:w="15840" w:h="12240" w:orient="landscape"/><w:pgMar w:top="' +
            (t = a.pageMargins || {
              top: 1800,
              right: 1440,
              bottom: 1800,
              left: 1440,
            }).top +
            '" w:right="' +
            t.right +
            '" w:bottom="' +
            t.bottom +
            '" w:left="' +
            t.left +
            '" w:header="720" w:footer="720" w:gutter="0"/><w:cols w:space="720"/><w:docGrid w:linePitch="360"/></w:sectPr>'
          : '<w:sectPr w:rsidR="00A02F19" w:rsidSect="00A02F19">' +
            (o.secPrExtra ? o.secPrExtra : "") +
            '<w:pgSz w:w="12240" w:h="15840"/><w:pgMar w:top="' +
            (t = a.pageMargins || {
              top: 1440,
              right: 1800,
              bottom: 1440,
              left: 1800,
            }).top +
            '" w:right="' +
            t.right +
            '" w:bottom="' +
            t.bottom +
            '" w:left="' +
            t.left +
            '" w:header="720" w:footer="720" w:gutter="0"/><w:cols w:space="720"/><w:docGrid w:linePitch="360"/></w:sectPr>'));
    return (r += e.docEndExtra + "</w:" + e.docType + ">");
  }
  (e.cbMakeDocxDocument = s),
    msdoc$2.makemsdoc(e, t, a, i, r),
    i.plugs.type.msoffice.makeOfficeGenerator("word", "document", {}),
    e.on("clearData", function () {
      e.data.length = 0;
    }),
    e.on("beforeGen", function () {
      i.features.type.docx.emitEvent("beforeGen", e),
        i.features.type.docx.emitEvent("beforeGenFinal", e);
    }),
    i.plugs.type.msoffice.addInfoType("dc:title", "", "title", "setDocTitle"),
    i.plugs.type.msoffice.addInfoType(
      "dc:subject",
      "",
      "subject",
      "setDocSubject"
    ),
    i.plugs.type.msoffice.addInfoType(
      "cp:keywords",
      "",
      "keywords",
      "setDocKeywords"
    ),
    i.plugs.type.msoffice.addInfoType(
      "dc:description",
      "",
      "description",
      "setDescription"
    ),
    i.plugs.type.msoffice.addInfoType(
      "cp:category",
      "",
      "category",
      "setDocCategory"
    ),
    i.plugs.type.msoffice.addInfoType(
      "cp:contentStatus",
      "",
      "status",
      "setDocStatus"
    );
  var n = new docplugman$2(e, i, "docx", function (e) {});
  n.plugsList.push(new plugHeadfoot(n)),
    a.extraPlugs &&
      "object" === _typeof(a.extraPlugs) &&
      a.extraPlugs.forEach &&
      a.extraPlugs.forEach(function (e) {
        var t;
        e &&
          ("function" == typeof e
            ? (t = e)
            : "string" == typeof e && (t = require("./" + e))),
          n.plugsList.push(new t(n));
      });
  var o = n.getDataStorage();
  return (
    i.type.msoffice.files_list.push(
      {
        name: "/word/settings.xml",
        type:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.settings+xml",
        clear: "type",
      },
      {
        name: "/word/fontTable.xml",
        type:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.fontTable+xml",
        clear: "type",
      },
      {
        name: "/word/webSettings.xml",
        type:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.webSettings+xml",
        clear: "type",
      },
      {
        name: "/word/styles.xml",
        type:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.styles+xml",
        clear: "type",
      },
      {
        name: "/word/document.xml",
        type:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.document.main+xml",
        clear: "type",
      },
      {
        name: "/word/numbering.xml",
        type:
          "application/vnd.openxmlformats-officedocument.wordprocessingml.numbering+xml",
        clear: "type",
      }
    ),
    i.type.msoffice.rels_app.push(
      {
        type:
          "http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles",
        target: "styles.xml",
        clear: "type",
      },
      {
        type:
          "http://schemas.openxmlformats.org/officeDocument/2006/relationships/settings",
        target: "settings.xml",
        clear: "type",
      },
      {
        type:
          "http://schemas.openxmlformats.org/officeDocument/2006/relationships/webSettings",
        target: "webSettings.xml",
        clear: "type",
      },
      {
        type:
          "http://schemas.openxmlformats.org/officeDocument/2006/relationships/fontTable",
        target: "fontTable.xml",
        clear: "type",
      },
      {
        type:
          "http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme",
        target: "theme/theme1.xml",
        clear: "type",
      },
      {
        type:
          "http://schemas.openxmlformats.org/officeDocument/2006/relationships/numbering",
        target: "numbering.xml",
        clear: "type",
      }
    ),
    (e.data = []),
    i.plugs.intAddAnyResourceToParse(
      "docProps\\app.xml",
      "buffer",
      null,
      function (t) {
        var a = e.options.author || e.options.creator || "officegen";
        return (
          i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(t) +
          '<Properties xmlns="http://schemas.openxmlformats.org/officeDocument/2006/extended-properties" xmlns:vt="http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes"><Template>Normal.dotm</Template><TotalTime>1</TotalTime><Pages>1</Pages><Words>0</Words><Characters>0</Characters><Application>Microsoft Office Word</Application><DocSecurity>0</DocSecurity><Lines>1</Lines><Paragraphs>1</Paragraphs><ScaleCrop>false</ScaleCrop><Company>' +
          a +
          "</Company><LinksUpToDate>false</LinksUpToDate><CharactersWithSpaces>0</CharactersWithSpaces><SharedDoc>false</SharedDoc><HyperlinksChanged>false</HyperlinksChanged><AppVersion>12.0000</AppVersion></Properties>"
        );
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "word\\fontTable.xml",
      "buffer",
      null,
      function (e) {
        return (
          i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
          '<w:fonts xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"><w:font w:name="Calibri"><w:panose1 w:val="020F0502020204030204"/><w:charset w:val="00"/><w:family w:val="swiss"/><w:pitch w:val="variable"/><w:sig w:usb0="A00002EF" w:usb1="4000207B" w:usb2="00000000" w:usb3="00000000" w:csb0="0000009F" w:csb1="00000000"/></w:font><w:font w:name="Arial"><w:panose1 w:val="020B0604020202020204"/><w:charset w:val="00"/><w:family w:val="swiss"/><w:pitch w:val="variable"/><w:sig w:usb0="20002A87" w:usb1="80000000" w:usb2="00000008" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/></w:font><w:font w:name="Times New Roman"><w:panose1 w:val="02020603050405020304"/><w:charset w:val="00"/><w:family w:val="roman"/><w:pitch w:val="variable"/><w:sig w:usb0="20002A87" w:usb1="80000000" w:usb2="00000008" w:usb3="00000000" w:csb0="000001FF" w:csb1="00000000"/></w:font><w:font w:name="Cambria"><w:panose1 w:val="02040503050406030204"/><w:charset w:val="00"/><w:family w:val="roman"/><w:pitch w:val="variable"/><w:sig w:usb0="A00002EF" w:usb1="4000004B" w:usb2="00000000" w:usb3="00000000" w:csb0="0000009F" w:csb1="00000000"/></w:font></w:fonts>'
        );
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "word\\settings.xml",
      "buffer",
      null,
      function (e) {
        return (
          i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
          '<w:settings xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:sl="http://schemas.openxmlformats.org/schemaLibrary/2006/main"><w:zoom w:percent="120"/><w:defaultTabStop w:val="720"/><w:characterSpacingControl w:val="doNotCompress"/><w:compat/><w:rsids><w:rsidRoot w:val="00A94AF2"/><w:rsid w:val="00A02F19"/><w:rsid w:val="00A94AF2"/></w:rsids><m:mathPr><m:mathFont m:val="Cambria Math"/><m:brkBin m:val="before"/><m:brkBinSub m:val="--"/><m:smallFrac m:val="off"/><m:dispDef/><m:lMargin m:val="0"/><m:rMargin m:val="0"/><m:defJc m:val="centerGroup"/><m:wrapIndent m:val="1440"/><m:intLim m:val="subSup"/><m:naryLim m:val="undOvr"/></m:mathPr><w:themeFontLang w:val="en-US" w:bidi="en-US"/><w:clrSchemeMapping w:bg1="light1" w:t1="dark1" w:bg2="light2" w:t2="dark2" w:accent1="accent1" w:accent2="accent2" w:accent3="accent3" w:accent4="accent4" w:accent5="accent5" w:accent6="accent6" w:hyperlink="hyperlink" w:followedHyperlink="followedHyperlink"/><w:shapeDefaults><o:shapedefaults v:ext="edit" spidmax="2050"/><o:shapelayout v:ext="edit"><o:idmap v:ext="edit" data="1"/></o:shapelayout></w:shapeDefaults><w:decimalSymbol w:val="."/><w:listSeparator w:val=","/></w:settings>'
        );
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "word\\webSettings.xml",
      "buffer",
      null,
      function (e) {
        return (
          i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
          '<w:webSettings xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"><w:optimizeForBrowser/></w:webSettings>'
        );
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "word\\styles.xml",
      "buffer",
      { styleXML: a.styleXML },
      function (e) {
        var t = (e && e.styleXML) || defaultStyleXML;
        return i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) + t;
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "word\\document.xml",
      "buffer",
      {
        docType: "document",
        docStartExtra: "<w:body>",
        docEndExtra: "</w:body>",
        data: e.data,
      },
      s,
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "word\\numbering.xml",
      "buffer",
      null,
      function (e) {
        return (
          i.plugs.type.msoffice.cbMakeMsOfficeBasicXml(e) +
          '<w:numbering xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns:mo="http://schemas.microsoft.com/office/mac/office/2008/main" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:mv="urn:schemas-microsoft-com:mac:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" mc:Ignorable="w14 w15 wp14"><w:abstractNum w:abstractNumId="0"><w:nsid w:val="709F643A"/><w:multiLevelType w:val="hybridMultilevel"/><w:tmpl w:val="B8B464D0"/><w:lvl w:ilvl="0" w:tplc="04090001"><w:start w:val="1"/><w:numFmt w:val="bullet"/><w:lvlText w:val=""/><w:lvlJc w:val="left"/><w:pPr><w:ind w:left="720" w:hanging="360"/></w:pPr><w:rPr><w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/></w:rPr></w:lvl><w:lvl w:ilvl="1" w:tplc="04090003" w:tentative="1"><w:start w:val="1"/><w:numFmt w:val="bullet"/><w:lvlText w:val="o"/><w:lvlJc w:val="left"/><w:pPr><w:ind w:left="1440" w:hanging="360"/></w:pPr><w:rPr><w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/></w:rPr></w:lvl><w:lvl w:ilvl="2" w:tplc="04090005" w:tentative="1"><w:start w:val="1"/><w:numFmt w:val="bullet"/><w:lvlText w:val=""/><w:lvlJc w:val="left"/><w:pPr><w:ind w:left="2160" w:hanging="360"/></w:pPr><w:rPr><w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/></w:rPr></w:lvl><w:lvl w:ilvl="3" w:tplc="04090001" w:tentative="1"><w:start w:val="1"/><w:numFmt w:val="bullet"/><w:lvlText w:val=""/><w:lvlJc w:val="left"/><w:pPr><w:ind w:left="2880" w:hanging="360"/></w:pPr><w:rPr><w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/></w:rPr></w:lvl><w:lvl w:ilvl="4" w:tplc="04090003" w:tentative="1"><w:start w:val="1"/><w:numFmt w:val="bullet"/><w:lvlText w:val="o"/><w:lvlJc w:val="left"/><w:pPr><w:ind w:left="3600" w:hanging="360"/></w:pPr><w:rPr><w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/></w:rPr></w:lvl><w:lvl w:ilvl="5" w:tplc="04090005" w:tentative="1"><w:start w:val="1"/><w:numFmt w:val="bullet"/><w:lvlText w:val=""/><w:lvlJc w:val="left"/><w:pPr><w:ind w:left="4320" w:hanging="360"/></w:pPr><w:rPr><w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/></w:rPr></w:lvl><w:lvl w:ilvl="6" w:tplc="04090001" w:tentative="1"><w:start w:val="1"/><w:numFmt w:val="bullet"/><w:lvlText w:val=""/><w:lvlJc w:val="left"/><w:pPr><w:ind w:left="5040" w:hanging="360"/></w:pPr><w:rPr><w:rFonts w:ascii="Symbol" w:hAnsi="Symbol" w:hint="default"/></w:rPr></w:lvl><w:lvl w:ilvl="7" w:tplc="04090003" w:tentative="1"><w:start w:val="1"/><w:numFmt w:val="bullet"/><w:lvlText w:val="o"/><w:lvlJc w:val="left"/><w:pPr><w:ind w:left="5760" w:hanging="360"/></w:pPr><w:rPr><w:rFonts w:ascii="Courier New" w:hAnsi="Courier New" w:cs="Courier New" w:hint="default"/></w:rPr></w:lvl><w:lvl w:ilvl="8" w:tplc="04090005" w:tentative="1"><w:start w:val="1"/><w:numFmt w:val="bullet"/><w:lvlText w:val=""/><w:lvlJc w:val="left"/><w:pPr><w:ind w:left="6480" w:hanging="360"/></w:pPr><w:rPr><w:rFonts w:ascii="Wingdings" w:hAnsi="Wingdings" w:hint="default"/></w:rPr></w:lvl></w:abstractNum><w:num w:numId="1"><w:abstractNumId w:val="0"/></w:num></w:numbering>'
        );
      },
      !0
    ),
    i.plugs.intAddAnyResourceToParse(
      "word\\_rels\\document.xml.rels",
      "buffer",
      i.type.msoffice.rels_app,
      i.plugs.type.msoffice.cbMakeRels,
      !0
    ),
    (e.createP = function (t) {
      return new docxP(e, i, "docx", e.data, {}, t);
    }),
    (e.createListOfDots = function (t) {
      var a = e.createP(t);
      return (a.options.list_type = "1"), a;
    }),
    (e.createListOfNumbers = function (t) {
      var a = e.createP(t);
      return (a.options.list_type = "2"), a;
    }),
    (e.putPageBreak = function () {
      var t = { data: [{ page_break: !0 }] };
      return (e.data[e.data.length] = t), t;
    }),
    (e.addPageBreak = function () {
      var t = { data: [{ page_break: !0 }] };
      return (e.data[e.data.length] = t), t;
    }),
    (e.createTable = function (t, a) {
      var i = e.createP(a);
      return (i.data = t), (i.type = "table"), i;
    }),
    (e.createJson = function (t, a) {
      switch (
        ("table" !== t.type && (a = a || e.createP(t.lopt || {})), t.type)
      ) {
        case "text":
          a.addText(t.val, t.opt);
          break;
        case "linebreak":
          a.addLineBreak();
          break;
        case "horizontalline":
          a.addHorizontalLine();
          break;
        case "image":
          a.addImage(t.path, t.opt || {}, t.imagetype);
          break;
        case "pagebreak":
          a = e.putPageBreak();
          break;
        case "table":
          a = e.createTable(t.val, t.opt);
          break;
        case "numlist":
          a = e.createListOfNumbers();
          break;
        case "dotlist":
          a = e.createListOfDots();
      }
      return a;
    }),
    (e.createByJson = function (t) {
      var a = {};
      return (
        (t = [].concat(t || [])).forEach(function (t) {
          Array.isArray(t)
            ? ((a = e.createP(t[0] || {})),
              t.forEach(function (t) {
                a = e.createJson(t, a);
              }))
            : (a = e.createJson(t));
        }),
        a
      );
    }),
    i.features.type.docx.emitEvent("makeDocApi", e),
    this
  );
}
baseobj$2.plugins.registerDocType(
  "docx",
  makeDocx,
  {},
  baseobj$2.docType.TEXT,
  "Microsoft Word Document"
);
var gendocx = Object.freeze({});
const name = "officegen",
  description =
    "Office Open XML Generator using Node.js streams. Supporting Microsoft Office 2007 and later Word (docx), PowerPoint (pptx,ppsx) and Excel (xlsx). This module is for all frameworks and environments. No need for any commandline tool - this module is doing everything inside it.",
  version = "0.4.8-0",
  author = { name: "Ziv Barber", url: "https://github.com/Ziv-Barber" },
  license = "MIT",
  url = "https://github.com/Ziv-Barber/officegen",
  engines = { node: ">= 0.10.0" },
  keywords = [
    "officegen",
    "office",
    "microsoft",
    "2007",
    "word",
    "powerpoint",
    "excel",
    "docx",
    "pptx",
    "ppsx",
    "xlsx",
    "charts",
    "Office Open XML",
    "stream",
    "generate",
    "create",
    "maker",
    "generator",
    "creator",
  ],
  repository = {
    type: "git",
    url: "git://github.com/Ziv-Barber/officegen.git",
  },
  bugs = { url: "https://github.com/Ziv-Barber/officegen/issues" },
  directories = { lib: "lib", examples: "examples" },
  scripts = {
    test: "mocha tests/",
    build: "npx rollup -c --environment BUILD:production",
    clean: "npx rimraf dist doc tmp",
    commit: "npx git-cz",
    esdoc: "npx esdoc",
    flow: "npx flow",
    lint: "better-npm-run lint",
  },
  dependencies = {
    archiver: "~3.0.0",
    async: "^2.6.1",
    "fast-image-size": "^0.1.3",
    jszip: "^2.5.0",
    lodash: "^4.17.11",
    "readable-stream": "~3.0.6",
    setimmediate: ">= 1.0.1",
    xmlbuilder: "^3.0.0",
  },
  devDependencies = {
    "adm-zip": "^0.4.11",
    "@babel/core": "^7.1.2",
    "@babel/node": "^7.0.0",
    "@babel/preset-env": "^7.1.0",
    "@babel/preset-flow": "^7.0.0",
    "babel-core": "^7.0.0-bridge.0",
    "babel-eslint": "^10.0.1",
    "babel-jest": "^23.6.0",
    "better-npm-run": "^0.1.1",
    chai: "^4.2.0",
    commitizen: "^3.0.4",
    "cz-conventional-changelog": "^2.1.0",
    esdoc: "^1.1.0",
    "esdoc-brand-plugin": "^1.0.1",
    "esdoc-ecmascript-proposal-plugin": "^1.0.0",
    "esdoc-flow-type-plugin": "^1.1.0",
    "esdoc-integrate-manual-plugin": "^1.0.0",
    "esdoc-integrate-test-plugin": "^1.0.0",
    "esdoc-lint-plugin": "^1.0.2",
    "esdoc-standard-plugin": "^1.0.0",
    eslint: "^5.7.0",
    "eslint-config-mocha": "^0.0.0",
    "eslint-config-standard": "^12.0.0",
    "eslint-plugin-flowtype": "^3.0.0",
    "eslint-plugin-import": "^2.14.0",
    "eslint-plugin-mocha": "^5.2.0",
    "eslint-plugin-mocha-only": "^0.0.3",
    "eslint-plugin-node": "^7.0.1",
    "eslint-plugin-promise": "^4.0.1",
    "eslint-plugin-standard": "^4.0.0",
    "flow-bin": "^0.84.0",
    "flow-typed": "^2.5.1",
    mocha: "^5.2.0",
    "npm-run-all": "^4.1.3",
    rimraf: "^2.6.2",
    rollup: "^0.66.6",
    "rollup-plugin-babel": "^4.0.3",
    "rollup-plugin-commonjs": "^9.2.0",
    "rollup-plugin-json": "^3.1.0",
    "rollup-plugin-node-resolve": "^3.4.0",
    "rollup-plugin-terser": "^3.0.0",
    sinon: "^7.1.0",
  },
  config = { commitizen: { path: "./node_modules/cz-conventional-changelog" } },
  amd = "dist/officegen.amd.js",
  main = "dist/officegen.cjs.js",
  module$1 = "dist/officegen.esm.js",
  betterScripts = {
    lint: { command: 'npx eslint "**/*.js"', env: { NODE_ENV: "development" } },
  };
var _package = {
    name: name,
    description: description,
    version: version,
    author: author,
    license: "MIT",
    url: url,
    engines: engines,
    keywords: keywords,
    repository: repository,
    bugs: bugs,
    directories: directories,
    scripts: scripts,
    dependencies: dependencies,
    devDependencies: devDependencies,
    config: config,
    amd: amd,
    main: main,
    module: module$1,
    betterScripts: betterScripts,
  },
  _package$1 = Object.freeze({
    name: name,
    description: description,
    version: version,
    author: author,
    license: "MIT",
    url: url,
    engines: engines,
    keywords: keywords,
    repository: repository,
    bugs: bugs,
    directories: directories,
    scripts: scripts,
    dependencies: dependencies,
    devDependencies: devDependencies,
    config: config,
    amd: amd,
    main: main,
    module: module$1,
    betterScripts: betterScripts,
    default: _package,
  }),
  require$$0 = getCjsExportFromNamespace(baseobj);
getCjsExportFromNamespace(genpptx),
  getCjsExportFromNamespace(genxlsx),
  getCjsExportFromNamespace(gendocx);
var officegen_info = getCjsExportFromNamespace(_package$1),
  officegen = createCommonjsModule(function (e) {
    (e.exports = require$$0).version = officegen_info.version;
  });
export default officegen;
//# sourceMappingURL=officegen.esm.js.map
